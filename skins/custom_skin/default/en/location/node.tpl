{* vim: set ts=2 sw=2 sts=2 et: *}

<li {printTagAttributes(getListContainerAttributes()):h}>

  {if:getLink()}
    <a href="{getLink()}" class="location-title"><span>{t(getName())}</span></a>
  {else:}
    <span class="location-text">{t(getName())}</span>
  {end:}

</li>

