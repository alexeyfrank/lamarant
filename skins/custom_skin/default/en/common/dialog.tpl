<div class="block block-block">
  <h2 class="fancy" IF="getHead()"><span>{t(getHead())}</span></h2>
  <div class="content"><widget template="{getBody()}" /></div>
</div>
