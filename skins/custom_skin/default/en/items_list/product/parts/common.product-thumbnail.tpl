{* vim: set ts=2 sw=2 sts=2 et: *}

{**
 * Item thumbnail
 *
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 *}
<a
  href="{buildURL(#product#,##,_ARRAY_(#product_id#^product.product_id,#category_id#^categoryId))}"
  class="product-thumbnail">
  {* <img src="{product.image.URL}" width="240" height="240" class="photo" /> *}
  <widget
    class="\XLite\View\Image"
    image="{product.getImage()}"
    maxWidth="{240}"
    maxHeight="{240}"
    alt="{product.name}"
    verticalAlign="top"
    className="photo" />
</a>
