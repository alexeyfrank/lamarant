{* vim: set ts=2 sw=2 sts=2 et: *}


<div class="row product-price widget-fingerprint-product-price">
    {if:product.old_price}
    <div class="col-xs-6">
      <span class="price product-price">{product.display_price} <i class="fa fa-rub"></i></span>
    </div>

    <div class="col-xs-6">
      <span class="price product-price old-price">{product.old_price} <i class="fa fa-rub"></i></span>
    </div>
    {else:}
      <div class="col-xs-12">
        <span class="price product-price">{product.display_price} <i class="fa fa-rub"></i></span>
      </div>
    {end:}
</div>
