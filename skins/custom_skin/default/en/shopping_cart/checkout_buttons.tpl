{* vim: set ts=2 sw=2 sts=2 et: *}
<div class="checkout-buttons">
  <list name="cart.buttons.checkout" />
</div>
