{* vim: set ts=2 sw=2 sts=2 et: *}

<li class="disabled-reason" IF="!cart.checkCart()">
  <div class="reason-details">{getDisabledReason():h}</div>
</li>
