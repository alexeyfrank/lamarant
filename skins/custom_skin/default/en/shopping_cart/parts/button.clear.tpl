{* vim: set ts=2 sw=2 sts=2 et: *}

<widget class="\XLite\View\Form\Cart\Clear" name="clearCart" />
  <div>
    <a
      href="{buildURL(#cart#,#clear#)}"
      onclick="javascript: return confirm('{t(#You are sure to clear your cart?#)}') && !jQuery(this).parents('form').eq(0).submit();"
      class="clear-bag">{t(#Clear bag#)}</a>
  </div>
<widget name="clearCart" end />
