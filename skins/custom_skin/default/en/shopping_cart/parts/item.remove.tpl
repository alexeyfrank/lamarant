{* vim: set ts=2 sw=2 sts=2 et: *}

<td class="item-remove delete-from-list">
  <widget class="\XLite\View\Form\Cart\Item\Delete" name="itemRemove{item.getItemId()}" item="{item}" />
    <div><widget class="\XLite\View\Button\Image" label="Delete item" style="remove" jsCode="return jQuery(this).closest('form').submit();" /></div>
  <widget name="itemRemove{item.getItemId()}" end />
</td>
