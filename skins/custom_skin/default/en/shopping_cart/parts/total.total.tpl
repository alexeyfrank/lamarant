{* vim: set ts=2 sw=2 sts=2 et: *}

<li class="total">
  <strong>{t(#Total#)}:</strong>
  <widget class="XLite\View\Surcharge" surcharge="{cart.getTotal()}" currency="{cart.getCurrency()}" />
</li>
