{* vim: set ts=2 sw=2 sts=2 et: *}

<table class="selected-products table table-stripped" cellspacing="0">

  <tbody class="items">
    <tr class="selected-product" FOREACH="cart.getItems(),item">
      <list name="cart.item" item="{item}" />
    </tr>
  </tbody>

  <tbody class="additional-items">
    <tr class="selected-product additional-item" FOREACH="getViewList(#cart.items#),w">
      {w.display()}
    </tr>
  </tbody>

</table>
