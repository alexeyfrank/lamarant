{* vim: set ts=2 sw=2 sts=2 et: *}

<p class="item-weight" IF="{item.getWeight()}">
  <span>{t(#Weight#)}:</span>
  {formatWeight(item.getWeight())}
</p>
