{* vim: set ts=2 sw=2 sts=2 et: *}

<p class="item-out-of-stock" IF="{!item.isValidAmount()}">{t(#Out of stock#)}</p>
