{* vim: set ts=2 sw=2 sts=2 et: *}

<td class="item-subtotal">
  <span class="subtotal{if:item.getExcludeSurcharges()} modified-subtotal{end:}"><widget class="XLite\View\Surcharge" surcharge="{item.getTotal()}" currency="{cart.getCurrency()}" /></span>
  <div IF="item.getExcludeSurcharges()" class="including-modifiers" style="display: none;">
    <table class="including-modifiers" cellspacing="0">
      <tr FOREACH="item.getExcludeSurcharges(),surcharge">
        <td class="name">{t(#Including X#,_ARRAY_(#name#^surcharge.getName()))}:</td>
        <td class="value"><widget class="XLite\View\Surcharge" surcharge="{surcharge.getValue()}" currency="{cart.getCurrency()}" /></td>
      </tr>
    </table>
  </div>
  <list name="cart.item.actions" item="{item}" />
</td>
