{* vim: set ts=2 sw=2 sts=2 et: *}

<td class="item-thumbnail" IF="item.hasImage()"><a href="{item.getURL()}"><widget class="\XLite\View\Image" image="{item.getImage()}" alt="{item.getName()}" maxWidth="80" maxHeight="80" centerImage="0" /></a></td>
<td class="item-thumbnail" IF="!item.hasImage()">&nbsp;</td>
