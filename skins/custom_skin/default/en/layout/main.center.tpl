{* vim: set ts=2 sw=2 sts=2 et: *}

<div class="center-main">
  <div class="container">
    {if:isForceChangePassword()}
    <div id="main" class="force-change-password-section clearfix">
      <widget class="\XLite\View\Model\Profile\ForceChangePassword" />
    </div>
    {else:}
    <div id="main" class="clearfix">

      <list name="layout.main.center" />
    </div>
    {end:}
  </div>
</div>
