{* vim: set ts=2 sw=2 sts=2 et: *}

<div class="footer">
  <list name="layout.main.footer" />
</div>


<script type="text/template" id="call-order-modal-tpl">
<div id="call-order-modal" class="modal fade lamarant">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
        <h4 class="modal-title">Оставьте ваши данные и мы вам перезвоним!</h4>
      </div>
      <div class="modal-body">
        <div class="success-message hide">
          <p>Спасибо, мы свяжемся с вами в самое ближайшее время!</p>
        </div>
        <form>
          <input type="hidden" name="target" value="call_orders">
          <input type="hidden" name="action" value="create">
          <div class="form-group">
            <label for="call_order_name">Ваше имя</label>
            <input type="text" class="form-control" name="name" id="call_order_name" placeholder="Ваше имя">
          </div>
          <div class="form-group">
            <label for="call_order_phone">Ваш телефон</label>
            <input type="text" class="form-control" name="phone" id="call_order_phone" placeholder="Ваш телефон">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn regular-button regular-main-button">Заказать</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter27128885 = new Ya.Metrika({
                    id:27128885,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27128885" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

