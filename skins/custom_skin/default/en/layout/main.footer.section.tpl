<div class="footer__top">
  <div class="container">
        <a href="{buildUrl()}" title="{t(#home#)}" rel="home"><img src="{getSmallLogo()}" alt="{t(#home#)}" /></a>
  </div>
</div>
<div class="footer__middle">
  <div class="container">
    <div class="col-md-3">
        <p class="left-call-text">Звоните</p>
    </div>
    <div class="col-md-4 middle">
        <div class="row">
            <div class="col-xs-2 left">
                <i class="glyphicon glyphicon-earphone"></i>
            </div>
            <div class="col-xs-10">
                <div class="row"><small>24 часа в сутки / 7 дней в неделю</small></div>
                <div class="row phone">
                    <small>8 (985)</small> <a href="tel:+79859160688">916 06 88</a>
                </div>
                <div class="row">
                    <a href="#" class="call-order-form">Заказать бесплатный звонок</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 right nopadding">
        <div class="row">
            <div class="col-xs-5 nopadding">
                <p class="right-call-text">Официальная группа</p>
            </div>
            <div class="col-xs-6">
                <a href="http://vk.com/lamarant" class="vk-button"></a>
            </div>
        </div>
        <span class="right-call-text"></span>
    </div>
  </div>
</div>

<div class="footer__bottom">
  <div class="container">
    <widget class="\XLite\View\Menu\Customer\Top" />
  </div>
</div>
