{* vim: set ts=2 sw=2 sts=2 et: *}

<div class="col-md-3 sidebar sidebar-right">
  <div class="section phone">
    <div class="row">
      <div class="col-xs-2">
        <i class="glyphicon glyphicon-earphone"></i>
      </div>
      <div class="col-xs-10 number">
        <div class="row">
         <small>8 (985)</small> <a href="tel:+79859160688">916 06 88</a>
        </div>
        <div class="row">
          <small>Звонок бесплатный!</small>
        </div>
      </div>
    </div>
    <div class="row">
      <a href="#" class="call-order-form">ЗАКАЗАТЬ ЗВОНОК</a>
    </div>
  </div>

  <div class="section look-book">
    <a href="/?target=lookbook"><img src="{getStaticImage(#common/look_book_right_menu.png#)}" class="img-responsive" alt="Responsive image"></a>
  </div>
  <div class="section banners">
    <a href="/?target=promotions"><img src="{getStaticImage(#banners/foto.png#)}" class="img-responsive" alt="Responsive image"></a>
  </div>

  <div class="section banners">
    <a href="/?target=promotions"><img src="{getStaticImage(#banners/raskaz.png#)}" class="img-responsive" alt="Responsive image"></a>
  </div>

  <div class="section review">
{foreach:getSiteReviewsForRightSidebar(),idx,review}
      <article class="media">
        <h5>{review.name} / {review.city}</h5>
        {if:review.getImage()}
          <img class="img-responsive" alt="{review.name}" src="{review.image.frontURL}">
        {end:}
        <p>{review.text}</p>
      </article>
{end:}
      <br />
    <a href="/?target=site_reviews" class="btn regular-button regular-main-button" style="display:table; margin: 0 auto;">Все отзывы</a>
  </div>

{*
  <div class="section">
    <list name="sidebar.second" />
  </div>
*}
</div>
