{* vim: set ts=2 sw=2 sts=2 et: *}

<div class="header">
  <div class="header-nav">
    <div class="bg"></div>
    <div class="container">
      <div class="col-md-3">
        <div class="logo">
          <a href="{buildURL()}" title="{t(#Home#)}" rel="home"><img src="{getLogo()}" alt="{t(#Home#)}" /></a>
        </div>
      </div>
      <div class="col-md-9">
        <div class="top-main-menu pull-right collapse navbar-collapse">
          <widget class="\XLite\Module\XC\CustomSkin\View\Menu\Customer\Top" />
        </div>
      </div>
    </div>
  </div>

  <div class="header-slider">
    <div class="header-slider-inner flexslider">
      <ul class="slides">
        <li><a href="/superpredlozheniya"><img src="{getStaticImage(#header/banner_snowfall2.jpg#)}" /></a></li>
        <li><a href="/superpredlozheniya"><img src="{getStaticImage(#header/banner_dollar.jpg#)}" /></a></li>
        <li><a href="/?target=about"><img src="{getStaticImage(#header/baner_lamarant.jpg#)}" /></a></li>
        <li><a href="/uggi"><img src="{getStaticImage(#header/baner_kolekzii2.jpg#)}" /></a></li>
        <li><a href="/?target=promotions"><img src="{getStaticImage(#header/baner_akzii.jpg#)}" /></a></li>
      </ul>
    </div>
  </div>
</div>

<widget class="\XLite\Module\XC\CustomSkin\View\MainMenu" displaymode="tree" is_subtree />
