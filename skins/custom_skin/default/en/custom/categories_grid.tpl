{foreach:getTopSubcategories(),top_idx,top_cat}
    {if:hasSubCategories(top_cat.category_id)}
    <h2 class="fancy"><span>{top_cat.name}</span></h2>
    <div class="block block-block">
        <div class="content">
            <div class="items-list category-products">
                <div class="products categories-grid">
                    <ul class="products-grid grid-list">
                        {foreach:getSubCategories(top_cat.category_id),idx,cat}
                            <li class="product-cell box-product">
                                <div class="product productid-{cat.category_id}">
                                    <div class="product-photo">

                                        <a href="{buildURL(#category#,##,_ARRAY_(#category_id#^cat.category_id))}" class="product-thumbnail">
                                          <widget
                                            class="\XLite\View\Image"
                                            image="{cat.getImage()}"
                                            height="244"
                                            alt="{cat.name}"
                                            verticalAlign="top"
                                            className="photo image-responsive" />
                                        </a>
                                    </div>
                                    {*
                                    <h3 class="product-name">
                                    <a class="fn url" href="novinki-1/ugg_bailey_bow_tall_navy.html">{cat.name}</a>
                                    </h3>
                                    *}
                                </div>
                            </li>
                        {end:}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {end:}
{end:}
