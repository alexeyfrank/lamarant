<div class="main-menu">
  <div class="main-menu__top">
    <div class="container">
      <div class="col-md-3">
        <a class="logo" href="{buildUrl()}" title="{t(#home#)}" rel="home"><img src="{getSmallLogo()}" alt="{t(#home#)}" /></a>
      </div>

      {foreach:getTopCategories(),idx,_category}
        <div {displayItemClass(idx,_categoryArraySize,_category,#col-md-3 nav-item#):h}>
          <span class="triangle"></span>
          <a href="{buildURL(#category#,##,_ARRAY_(#category_id#^_category.category_id))}" {displayLinkClass(idx,_categoryArraySize,_category):h} data-menu="{_category.categoryId}">
              <span class="icon"></span>
          </a>
        </div>
      {end:}

      <div class="col-md-3">
        <a href="{buildUrl(#cart#)}" class="cart lc-minicart">
            <span class="icon"></span>Корзина <strong>(<span class="minicart-items-number-text minicart-items-number">{cart.countQuantity()}</span>)</strong>
        </a>
      </div>
    </div>
  </div>

  {foreach:getTopCategories(),topIdx,topCategory}
  <div class="main-menu__bottom" data-submenu="{topCategory.categoryId}" data-active="{isActiveTrail(topCategory)}">
    <div class="container">
        <ul class="nav nav-pills VMmenu">
            {foreach:getSubCategories(topCategory.id),idx,category}
                <li {displayItemClass(idx,_categoryArraySize,category,##):h}>
                  <a href="{buildURL(#category#,##,_ARRAY_(#category_id#^category.category_id))}" {displayLinkClass(innerIdx,_categoryArraySize,category):h}>
                    <strong>{category.getName()}</strong>
                  </a>
                  {if:hasSubCategories(category.id)}
                    <div>
                        <ul class="menu">
                        {foreach:getSubCategories(category.id),innerIdx,c}
                          <li {displayItemClass(idx,_categoryArraySize,c,##):h}>
                            <div>
                              <a href="{buildURL(#category#,##,_ARRAY_(#category_id#^c.category_id))}" {displayLinkClass(innerIdx,_categoryArraySize,c):h}>
                                <img class="floatleft" src="{c.menuImage.frontURL}" alt="{c.name}">
                                {c.getName()}
                                <p>{c.getMenuSubtitle()}</p>
                              </a>
                            </div>
                          </li>
                        {end:}
                        </ul>
                    </div>

                  {end:}
                </li>
           {end:}
        </ul>
    </div>
  </div>
  {end:}
</div>
