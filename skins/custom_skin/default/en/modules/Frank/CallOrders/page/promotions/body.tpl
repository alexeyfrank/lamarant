{* vim: set ts=2 sw=2 sts=2 et: *}

<h1 class="original">Расскажи друзьям и получи 500 рублей</h1>

<img style="margin: 10px; float:right;" src="{getStaticImage(#promotions/raskazat.jpg#)}" class="img-responsive" style="float: right;">
<p>Получи <span class="pink">500 рублей</span> скидки на первую покупку, для этого всего лишь нужно сделать у себя на стене <span class="pink">репост нашей записи</span>. По условиям акции запись должна провисеть в топе (быть первой) на вашей страницы 7 дней.</p>

<div class="socials">
  <span class='st_vkontakte_large' displayText='Vkontakte'></span>
  <span class='st_facebook_large' displayText='Facebook'></span>
  <span class='st_twitter_large' displayText='Tweet'></span>
  <span class='st_googleplus_large' displayText='Google +'></span>
  <span class='st_odnoklassniki_large' displayText='Odnoklassniki'></span>
</div>

<div style="clear:both;"></div>
<br />

<h1 class="original">Купим ваши фото за 700 рублей</h1>
<img style="margin: 10px; float: right;" src="{getStaticImage(#promotions/foto.jpg#)}" class="img-responsive" style="float: right;">
<p><a href="http://lamarant.ru" title="LaMarant.ru">LaMarant.ru</a> запускает уникальную акцию! Теперь покупая в нашем магазине брендовые угги UGG Australia или кеды Isabel Marant у Вас есть возможность получить <span class="pink">700 рублей</span> скидки на любую следующую покупку, для этого всего лишь нужно прислать <span class="pink">Вашу фотографию в нашей обуви</span>! Акция действуют на всю коллекцию магазина!</p>

<p>Фотографию можно прислать на наш электронный адрес foto@lamarant.ru или загрузить в раздел <a href="/?target=site_reviews">отзывы</a>!</p>

<p><strong>ВНИМАНИЕ:</strong></p>

<p>Каждый месяц среди покупателей проводится конкурс на лучшую фотографию, по итогам конкурса победитель получает 1 пару угг UGG Australia или кед Isabel Marant на выбор в подарок!</p>
<p>Присылая фотографию на почту, просим указать Ваше имя, дату покупки и номер телефона.</p>

