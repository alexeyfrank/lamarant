{* vim: set ts=2 sw=2 sts=2 et: *}

<h1 class="original">Ваши отзывы</h1>

<div class="site-reviews-list">
{foreach:getSiteReviews(),idx,review}
  <article class="media">
    {if:review.getImage()}
    <a class="media-left" href="#">
      <img class="media-left" alt="{review.name}" src="{review.image.frontURL}" style="width: 96px; height: 96px;">
    </a>
    {end:}
    <div class="media-body">
      <h4 class="media-heading">{review.name} / {review.city}</h4>
      <p>{review.text}</p>
      <time><i>{formatDate(review.date, #%m/%d/%Y#)}</i></time>
    </div>
  </article>
{end:}
</div>


<button class="btn regular-button regular-main-button site-review-form">Оставить отзыв</a>

<script type="text/template" id="site-review-modal-tpl">
<div id="site-review-modal" class="modal fade lamarant">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
        <h4 class="modal-title">Оставьте свой отзыв!</h4>
      </div>
      <div class="modal-body">
        <div class="success-message hide">
          <p>Спасибо, в целях ограничения спама ваш отзыв принят на модерацию!</p>
        </div>
        <form>
          <input type="hidden" name="target" value="site_reviews">
          <input type="hidden" name="action" value="create">
          <div class="form-group">
            <label for="site_review_name">Ваше имя</label>
            <input type="text" class="form-control" name="name" id="site_review_name" placeholder="Ваше имя">
          </div>
          <div class="form-group">
            <label for="site_review_image">Ваше фото</label>
            <input type="file" class="form-control" name="image" id="site_review_image" placeholder="Ваше фото">
          </div>
          <div class="form-group">
            <label for="site_review_city">Ваш город</label>
            <input type="text" class="form-control" name="city" id="site_review_city" placeholder="Ваш город">
          </div>
          <div class="form-group">
            <label for="site_review_text">Ваш отзыв</label>
            <textarea name="text" class="form-control" id="site_review_text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn regular-button regular-main-button">Оставить отзыв</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</script>

