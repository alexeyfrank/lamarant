{* vim: set ts=2 sw=2 sts=2 et: *}

<div class="steps clearfix">
  <div
    FOREACH="getSteps(),stepKey,step"
    class="step
      {stepKey}-step
      {if:!isEnabledStep(step)}disabled{end:}
    ">
    <div class="step-box clearfix">
      <h2>
        <span class="text">{t(step.getTitle())}</span>
      </h2>
      {step.display()}
    </div>
  </div>

</div>

