{* vim: set ts=2 sw=2 sts=2 et: *}

<list name="jscontainer.js">
<script src="//cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/jquery.flexslider-min.js"></script>
<widget template="jscontainer/custom/is_mobile.tpl" />
<widget template="jscontainer/custom/tap.tpl" />
<widget template="jscontainer/custom/minicart.tpl" />
<widget template="jscontainer/custom/main.tpl" />
