{* vim: set ts=2 sw=2 sts=2 et: *}

{if:!doJSAggregation()}
<script FOREACH="getJSResources(),file" type="text/javascript" src="{getResourceURL(file.url)}"></script>
{end:}
