<script>

function initMainMenuScroll() {
    if (isMobile.any) {
        return;
    }
    var nav = $('.main-menu');
    var NAV_TOP = nav.position().top;

    $(window).scroll(function () {
            if ($(this).scrollTop() > NAV_TOP) {
            nav.addClass("navbar-fixed-top");
            nav.next().css({ marginTop: nav.height() + "px" });
            } else {
            nav.removeClass("navbar-fixed-top");
            nav.next().css({ marginTop: "0px" });
            }
            });
}
function initMainMenuToggle() {
    if (isMobile.any) {
        $(".VMmenu > li > a").on("tap", function() { return false; });
        $("body").on("click", ".VMmenu > li > a", function() { return false; });
    }

    $(".main-menu__bottom[data-active='1']").show();
    if ($(".main-menu__bottom[data-active='1']").length == 0) {
        $(".main-menu__bottom:first").show();
    }

    $('.main-menu__top .nav-item a').click(function() {
            var id = $(this).data('menu');
            var $currentSubMenu = $(".main-menu__bottom[data-submenu='" + id + "']");
            if ($currentSubMenu.is(":visible") || $currentSubMenu.find("a").length == 0) {
            return true;
            }

            $(".main-menu__bottom:not([data-submenu='" + id + "'])").hide();
            $currentSubMenu.show();
            return false;
            });
}


function initCallOrderForm() {
    $('.call-order-form').click(function() {
            $("#call-order-modal").remove();
            var modal = $("#call-order-modal-tpl").html();
            $("body").append(modal);

            $callOrderModal = $("#call-order-modal");
            $callOrderModal.modal("show");

            $callOrderModal.find('[type="submit"]').click(function() {
                var form = $callOrderModal.find("form").serializeObject();
                $.post("/index.php?target=call_orders", form).then(function() {
                    $callOrderModal.find("form").hide();
                    $callOrderModal.find("button").hide();
                    $callOrderModal.find(".success-message").removeClass("hide");
                    });
                return false;
                });

            return false;
            });
}

function initSiteReviewForm() {
    $('.site-review-form').click(function() {
            $("#site-review-modal").remove();
            var modalTpl = $("#site-review-modal-tpl").html();
            $("body").append(modalTpl);

            var $modal = $("#site-review-modal");
            $modal.modal("show");

            $modal.find('[type="submit"]').click(function() {
                var form = $modal.find("form").serializeObject();
                $.post("/index.php?target=site_reviews", form).then(function() {
                    $modal.find("form").hide();
                    $modal.find("button").hide();
                    $modal.find(".success-message").removeClass("hide");
                    });
                return false;
                });

            return false;
            });
}

$.fn.serializeFormData = function() {
    var form = this[0];
    _.each(form.elements, function(el) {
            console.log(el);
            });
}

function prepareHelpers() {
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
                if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
                } else {
                o[this.name] = this.value || '';
                }
                });
        return o;
    };
}

prepareHelpers();


initMainMenuToggle();
initMainMenuScroll();
initCallOrderForm();
initSiteReviewForm();

$(".flexslider").flexslider();
</script>
