{* vim: set ts=2 sw=2 sts=2 et: *}
{if:getPageTitle()}
<title>LaMarant.ru | {t(getPageTitle())}</title>
{else:}
<title>LaMarant.ru</title>
{end:}

{if:getPageTitle()}
<meta property="og:title" content="Lamarant | {getPageTitle()}" />
{else:}
<meta property="og:title" content="Lamarant" />
{end:}
<meta property="og:url" content="{getShopURL(getURL())}" />
<meta property="og:image" content="{getShopURL(getLogo())}" />
<meta property="og:description" content="Интернет магазин угги в Москве «La marant» уже не первый год занимается реализацией качественных сапожек из натуральной кожи и овечьей шерсти, которые поставляются непосредственно от компании-производителя. Мы говорим НЕТ китайским подделкам из синтетики. Поэтому у нас каждый сможет найти товар себе по вкусу и подходящий по цене. Недорогие позиции – это тоже наш конек." />
