

<div class="size-select">
    <p class="title">Укажите размер:</p>
    {foreach:getAttrValue(),v}
        <input type="radio" id="radio_{v.id}" checked="{isSelectedValue(v)}" value="{v.id}" name="{getName()}" />
        <label for="radio_{v.id}">
            {getOptionTitle(v)}{getModifierTitle(v):h}
        </label>
    {end:}
</div>

{*
<select class="form-control" name="{getName()}" data-attribute-id="{attribute.id}">
  <option FOREACH="getAttrValue(),v" selected="{isSelectedValue(v)}" value="{v.id}">{getOptionTitle(v)}{getModifierTitle(v):h}</option>
</select>
*}
