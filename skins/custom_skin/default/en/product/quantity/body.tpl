<span class="{getCSSClass()} {getFingerprint()}">
{if:!isOutOfStock()}
  {t(#Qty#)}: <widget class="\XLite\View\Product\QuantityBox" fieldValue="{getQuantity()}" product="{getProduct()}" maxValue="{getMaxQuantity()}" />
{end:}
</span>
