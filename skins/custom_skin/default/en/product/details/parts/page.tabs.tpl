{if:false}
<div IF="getTabs()" class="product-details-tabs">

  <div class="tabs">
    <ul class="tabs primary">
      <li FOREACH="getTabs(),index,tab" class="{getTabClass(tab)}">
        <a data-id="{tab.id:h}" href="#{tab.id:h}">{t(tab.name)}</a>
      </li>
    </ul>
  </div>

  <div class="tabs-container">
  <div FOREACH="getTabs(),tab" id="{tab.id:h}" class="tab-container" style="{getTabStyle(tab)}">
    <a name="{tab.id:h}"></a>
    {if:tab.template}
      <widget template="{tab.template}" />

    {else:}
      {if:tab.widget}
        <widget class="{tab.widget}" product="{product}" />

      {else:}
        {if:tab.list}
          <list name="{tab.list}" product="{product}" />
        {else:}
          {if:tab.widgetObject}
            {tab.widgetObject.display():h}
          {end:}
        {end:}
      {end:}
    {end:}
  </div>
  </div>

</div>

{end:}
