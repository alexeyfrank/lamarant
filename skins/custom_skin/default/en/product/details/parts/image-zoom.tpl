<div class="product-photo">
  <a href="{getZoomImageURL()}" class="cloud-zoom" id="pimage_{product.product_id}" rel="adjustX: {getZoomAdjustX()}, showTitle: false, tintOpacity: 0.5, tint: '#fff', lensOpacity: 0, zoomWidth: 600, zoomHeight: 600" title="{t(#Thumbnail#)}">
  <widget class="\XLite\View\Image" image="{product.getImage()}" className="photo product-thumbnail" id="product_image_{product.product_id}" verticalAlign="top" alt="{t(#Thumbnail#)}" maxWidth="{getWidgetMaxWidth()}" maxHeight="{getWidgetMaxHeight()}" />
{displayCommentedData(getJSData())}
  </a>
</div>
