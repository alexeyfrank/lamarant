<div class="product-details-info">

  <h1 class="fn title">{product.name}</h1>
  <p class="sku">Артикул: {product.sku}</p>

  <div class="row">
    <div class="col-xs-5 new-price">
        <p class="name">Новая цена:</p>
        <p class="price">{product.display_price} <i class="fa fa-rub"></i></p>
    </div>

    <div class="col-xs-7 nopadding shade-base">
      <widget class="\XLite\View\Product\Details\Customer\AddButton" product="{product}" />
    </div>
  </div>

  {if:product.old_price}
      <div class="row">
        <div class="col-xs-5 old-price">
          <p class="name">Старая цена:</p>
          <p class="price">{product.old_price} <i class="fa fa-rub"></i><p>
        </div>
        <div class="col-xs-3 old-price nopadding">
          <p class="name">Скидка:</p>
          <p class="price no-through">{product.product_discount}%<p>
        </div>
        <div class="col-xs-4 nopadding old-price">
          <p class="name">Экономия:</p>
          <p class="price no-through">{product.product_saving} <i class="fa fa-rub"></i><p>
        </div>
      </div>
  {end:}

  <div class="row shipping">
    <div class="col-xs-2">
        <i class="fa fa-suitcase"></i>
    </div>
    <div class="col-xs-10 nopadding">
        <p><a href="/?target=shipping">Бесплатная доставка</a></p>
        <p><small>По москве (выгодная в регионы)</small></p>
    </div>

  </div>

  <div class="row">
      <div class="col-xs-12">
          <div class="attribute" FOREACH="product.getEditableAttributes(),attribute">
              <widget class="{attribute.getWidgetClass(attribute.getType())}" attribute="{attribute}" />
          </div>
      </div>
  </div>

  {if:isProductAdded()}
    <p class="product-added-note">
      <i class="fa fa-check-square"></i>
      {t(#This product has been added to your bag#,_ARRAY_(#href#^buildURL(#cart#))):h}
    </p>
  {end:}

  <div class="description">{product.description:h}</div>

</div>

