<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\XC\CustomSkin\View;

/**
 * Abstract widget
 */
abstract class AView extends \XLite\View\AView implements \XLite\Base\IDecorator
{

    private function getCssFile($name) {
        return 'modules/XC/CustomSkin/css/'. $name . '.css';
    }

    public function getCSSFiles()
    {
        $list = parent::getCSSFiles();
        $list[] = $this->getCssFile("main");
        $list[] = $this->getCssFile("header");
        $list[] = $this->getCssFile("main_menu");
        $list[] = $this->getCssFile("footer");
        $list[] = $this->getCssFile("products_list");
        $list[] = $this->getCssFile("product_view");
        $list[] = $this->getCssFile("cart");
        $list[] = $this->getCssFile("checkout");
        $list[] = $this->getCssFile("checkout_report");
        $list[] = $this->getCssFile("text");
        $list[] = $this->getCssFile("order");
        $list[] = $this->getCssFile("sidebar_right");
        return $list;
    }

    public function getSmallLogo()
    {
        return \XLite\Core\Layout::getInstance()->getResourceWebPath(
            'images/small_logo.png',
            \XLite\Core\Layout::WEB_PATH_OUTPUT_URL,
            \XLite::CUSTOMER_INTERFACE
        );
    }

    public function getStaticImage($path) {
        return \XLite\Core\Layout::getInstance()->getResourceWebPath(
            'images/'.$path,
            \XLite\Core\Layout::WEB_PATH_OUTPUT_URL,
            \XLite::CUSTOMER_INTERFACE
        );
    }

    protected function isSubtree()
    {
        return true;
    }

    protected function isFrontPage() {
        return \XLite\Core\Request::getInstance()->target == 'main';
    }


    protected function getSiteReviewsForRightSidebar()
    {
        return \XLite\Core\Database::getRepo('\XLite\Module\Frank\CallOrders\Model\SiteReview')->getPublishedOrderByDesc()->setMaxResults(2)->getResult();
    }
}
