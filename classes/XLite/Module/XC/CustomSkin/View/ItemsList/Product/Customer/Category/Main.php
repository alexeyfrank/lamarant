<?php
namespace XLite\Module\XC\CustomSkin\View\ItemsList\Product\Customer\Category;

class Main extends \XLite\View\ItemsList\Product\Customer\Category\Main
{
    protected static $PER_PAGE = "per_page";

    protected $cat_id = 0;

    public function __construct(array $params = array()) {
        $this->cat_id = $params[static::PARAM_CATEGORY_ID];
        parent::__construct($params);
    }

    public static function getAllowedTargets()
    {
        $target = parent::getAllowedTargets();
        $target[] = "product";
        $target[] = "category";
        $target[] = "main";
        return $target;
    }

    protected function getData(\XLite\Core\CommonCell $cnd, $countOnly = false)
    {
        $cnd = \XLite\Model\Repo\Base\Searchable::addLimitCondition(0, 6, $cnd);
        $category = $this->getCategory();

        return $category ? $category->getProducts($cnd, $countOnly) : null;
    }

    protected function getCategoryId() {
        $param = $this->cat_id ? $this->cat_id : \XLite\Core\Request::getInstance()->category_id;
        return $param;
    }

    protected function isVisible() {
        return true;
    }

    protected function getSidebarMaxItems() {
        return $this->getParam(self::PER_PAGE) || 6;
    }

    protected function isCacheAllowed() { return false; }
    protected function isCacheAvailable() { return false; }
}
