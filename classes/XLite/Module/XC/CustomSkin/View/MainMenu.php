<?php

namespace XLite\Module\XC\CustomSkin\View;

class MainMenu extends \XLite\View\TopCategories {
    protected function getDefaultTemplate()
    {
        return 'custom/main_menu.tpl';
    }

    protected function assembleItemClassName($index, $count, \XLite\Model\Category $category) {
        $classes = parent::assembleItemClassName($index, $count, $category);
        return $classes . ' category-id-' . $category->getId();
    }

    public function getTopCategories() {
        return array_values(array_filter($this->getCategories(), function($item) {
            return in_array($item->getCategoryId(), [9, 11]);
        }));
    }

    public function displayItemClass($index, $count, \XLite\Model\Category $category, $addClass)
    {
        $className = $this->assembleItemClassName($index, $count, $category);
        $firstClass = $index == 0 ? " first " : "";
        $formattedString = ' class="' . $className . ' ' . $addClass . '"'. $firstClass;
        return $className ?  $formattedString : '';
    }

    protected function getSubCategories($id = null)
    {
        return \XLite\Core\Database::getRepo('\XLite\Model\Category')->getSubcategories($id);
    }

    protected function hasSubCategories($id) {
        $count = count(\XLite\Core\Database::getRepo('\XLite\Model\Category')->getSubcategories($id));
        return $count > 0;
    }

    protected function getTopSubcategories($id) {
        $repo = \XLite\Core\Database::getRepo('\XLite\Model\Category');
        $result = array();
        $topCategories = $repo->getSubcategories($id);
        for ($i = 0; $i < count($topCategories); $i++) {
            $topCat = $topCategories[$i];
            $result = array_merge($result, array($topCat), $repo->getSubcategories($topCat->getId()));
        }

        return array_chunk($result, ceil(count($result) / 4));
    }

    protected function isTopSubcategory($topId, $category) {
        return $topId == $category->getParentId();
    }

    protected function getActiveCategoryId() {
        $rootId = $categoryId ?: $this->getParam(self::PARAM_ROOT_ID);
        $topCategories = \XLite\Core\Database::getRepo('\XLite\Model\Category')->getSubcategories($rootId);

        foreach ($topCategories as $category) {
            if ($this->isActiveTrail($category)) {
                return $category->getCategoryId();
            }
        }

        return -1;
    }
}
