<?php

namespace XLite\Module\XC\CustomSkin\View;

class CategoriesGrid extends \XLite\View\AView {
    protected function getDefaultTemplate()
    {
        return 'custom/categories_grid.tpl';
    }

    protected function getCategoryId() {
        return \XLite\Core\Request::getInstance()->category_id;
    }

    protected function isVisible() {
        return in_array($this->getCategoryId(), [9, 11]);
    }

    protected function getTopCategory() {
        return \XLite\Core\Database::getRepo('XLite\Model\Category')->getCategory($this->getCategoryId());
    }

    protected function getTopSubcategories()  {
        return $this->getSubCategories($this->getCategoryId());
    }

    protected function getSubCategories($id = null)
    {
        return \XLite\Core\Database::getRepo('\XLite\Model\Category')->getSubcategories($id);
    }

    protected function hasSubCategories($id) {
        $count = count(\XLite\Core\Database::getRepo('\XLite\Model\Category')->getSubcategories($id));
        return $count > 0;
    }

    protected function getActiveCategoryId() {
        $rootId = $categoryId ?: $this->getParam(self::PARAM_ROOT_ID);
        $topCategories = \XLite\Core\Database::getRepo('\XLite\Model\Category')->getSubcategories($rootId);

        foreach ($topCategories as $category) {
            if ($this->isActiveTrail($category)) {
                return $category->getCategoryId();
            }
        }

        return -1;
    }
}
