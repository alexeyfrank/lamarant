<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\CDev\XPaymentsConnector\Model;

/**
 * XPayments payment processor
 *
 */
class Order extends \XLite\Model\Order implements \XLite\Base\IDecorator
{
    /** 
     * Fraud statuses
     */
    const FRAUD_STATUS_CLEAN    = 'Clean';
    const FRAUD_STATUS_FRAUD    = 'Fraud';
    const FRAUD_STATUS_REVIEW   = 'Review';
    const FRAUD_STATUS_UNKNOWN  = '';

    /**
     * Order fraud status
     *
     * @var string
     *
     * @Column (type="string")
     */
    protected $fraud_status = '';

    /**
     * Get visible payment methods
     *
     * @return array
     */
    public function getCCData()
    {
        $result = array();

        foreach ($this->getPaymentTransactions() as $transaction) {

            if ($transaction->getCard()) {
                $result[] = $transaction->getCard();
            }

        }

        return $result;
    }

    /**
     * Set order status by transaction
     *
     * @param \XLite\Model\Payment\transaction $transaction Transaction which changes status
     *
     * @return void
     */
    public function setPaymentStatusByTransaction(\XLite\Model\Payment\transaction $transaction)
    {
        if (
            $transaction->isXpc(true)
            && $transaction->getDataCell('xpc_authorized')
            && $transaction->getDataCell('xpc_paid')
            && $transaction->getDataCell('xpc_refunded')
            && $transaction->getDataCell('xpc_voided')
        ) {

            $config = \XLite\Core\Config::getInstance()->CDev->XPaymentsConnector;

            $authorized = $transaction->getDataCell('xpc_authorized')->getValue();
            $captured = $transaction->getDataCell('xpc_paid')->getValue();
            $refunded = $transaction->getDataCell('xpc_refunded')->getValue();
            $voided = $transaction->getDataCell('xpc_voided')->getValue();

            if ($voided > 0) {

                $status = $config->xpc_status_declined;

            } elseif ($refunded > 0) {

                if ($refunded >= $captured) {
                    $status = $config->xpc_status_refunded; 
                } else {
                    $status = $config->xpc_status_refunded_part;
                }

            } elseif ($captured > 0) {

                if ($captured >= $authorized) {
                    $status = $config->xpc_status_charged;
                } else {
                    $status = $config->xpc_status_charged_part;
                }

            } elseif ($authorized > 0) {

                $status = $config->xpc_status_auth;

            } else {

                $status = $config->xpc_status_new;

            } 

            $this->setPaymentStatus($status);

        } else {
            parent::setPaymentStatusByTransaction($transaction);
        }

    }

    /**
     * Get array of payment transaction sums (how much is authorized, captured and refunded)
     *
     * @return array
     */
    public function getPaymentTransactionSums()
    {
        static $paymentTransactionSums = null;

        if (!isset($paymentTransactionSums)) {

            $authorized = 0;
            $captured = 0;
            $refunded = 0;
            $voided = 0;
            $xpcFound = false;

            $transactions = $this->getPaymentTransactions();

            foreach ($transactions as $t) {

                if (
                    $t->isXpc(true)
                    && $t->getDataCell('xpc_authorized')
                    && $t->getDataCell('xpc_paid')
                    && $t->getDataCell('xpc_refunded')
                    && $t->getDataCell('xpc_voided')
                ) {
                    $xpcFound = true;

                    $authorized += $t->getDataCell('xpc_authorized')->getValue();
                    $captured += $t->getDataCell('xpc_paid')->getValue();
                    $refunded += $t->getDataCell('xpc_refunded')->getValue();
                    $voided+= $t->getDataCell('xpc_voided')->getValue();
                }

            }

            if (!$xpcFound) {

                $paymentTransactionSums = parent::getPaymentTransactionSums();

            } else {
            
                $paymentTransactionSums = array(
                    static::t('Authorized amount') => $authorized,
                    static::t('Captured amount')   => $captured,
                    static::t('Refunded amount')   => $refunded,
                    static::t('Declined amount')   => $voided,
                );

            }

            if (
                $xpcFound
                || $this->hasSavedCardsInProfile()
            ) {

                $paymentTransactionSums[static::t('Difference')] = $this->getAomTotalDifference();

                // Remove from array all zero sums
                foreach ($paymentTransactionSums as $k => $v) {
                    if ($v <= self::ORDER_ZERO) {
                        unset($paymentTransactionSums[$k]);
                    }
                }
            }
        }

        return $paymentTransactionSums;
    }

    /**
     * Difference in order Total after AOM changes if (any)
     *
     * @return float
     */
    public function getAomTotalDifference()
    {
        // Apparently we'll need to change this
        return $this->getOpenTotal();
    }

    /**
     * Check if total difference after AOM changes is greater than zero
     *
     * @return boolean
     */
    public function isAomTotalDifferencePositive()
    {
        return $this->getAomTotalDifference() > \XLite\Model\Order::ORDER_ZERO;
    }

    /**
     * Does profile has saved cards 
     *
     * @return boolean
     */
    protected function hasSavedCardsInProfile()
    {
        return $this->getProfile()
            && $this->getProfile()->getSavedCards();
    }

    /**
     * Is recharge allowed for the order 
     *
     * @return boolean
     */
    public function isAllowRecharge()
    {
        return $this->isAomTotalDifferencePositive()
            && $this->hasSavedCardsInProfile(); 
    }

    /**
     * Return anchor name for the information about fraud check on the order details page
     *
     * @return string 
     */
    public function getFraudInfoAnchor()
    {
        return 'fraud-info-kount';
    }
}
