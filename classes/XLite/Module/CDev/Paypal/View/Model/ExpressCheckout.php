<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\CDev\Paypal\View\Model;

/**
 * ExpressCheckout
 */
class ExpressCheckout extends \XLite\Module\CDev\Paypal\View\Model\ASettings
{
    /**
     * Schema of the "Your account settings" section
     *
     * @var array
     */
    protected $schemaAccount = array(
        'section_merchant_sep' => array(
            self::SCHEMA_CLASS    => 'XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable',
            self::SCHEMA_LABEL    => 'E-Mail address to receive PayPal payment',
            \XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable::PARAM_GROUP_NAME => 'api_type',
            \XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable::PARAM_SELECTED => true,
        ),
        'email' => array(
            self::SCHEMA_CLASS    => '\XLite\View\FormField\Input\Text\Email',
            self::SCHEMA_LABEL    => '',
            self::SCHEMA_HELP     => 'Start accepting Express Checkout payments immediately by simply plugging in the email address where you would like to receive payments. You can create your PayPal account later after you have received a real transaction from your customers!',
            self::SCHEMA_REQUIRED => true,
            \XLite\View\FormField\AFormField::PARAM_USE_COLON => false,
        ),
        'section_payflow_sep' => array(
            self::SCHEMA_CLASS    => 'XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable',
            self::SCHEMA_LABEL    => 'API credentials for payments and post-checkout operations',
            self::SCHEMA_HELP     => 'Can be set up later',
            \XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable::PARAM_GROUP_NAME => 'api_type',
            \XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable::PARAM_SELECTED => false,
        ),
        'partner' => array(
            self::SCHEMA_CLASS    => '\XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Partner name',
            self::SCHEMA_HELP     => 'Your partner name is PayPal.',
            self::SCHEMA_REQUIRED => true,
        ),
        'vendor' => array(
            self::SCHEMA_CLASS    => '\XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Merchant login',
            self::SCHEMA_HELP     => 'This is the login name you created when signing up for PayPal Payments Advanced.',
            self::SCHEMA_REQUIRED => true,
        ),
        'user' => array(
            self::SCHEMA_CLASS    => '\XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'User',
            self::SCHEMA_HELP     => 'PayPal recommends entering a User Login here instead of your Merchant Login.',
            self::SCHEMA_REQUIRED => true,
        ),
        'pwd' => array(
            self::SCHEMA_CLASS    => '\XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Password',
            self::SCHEMA_HELP     => 'This is the password you created when signing up for PayPal Payments Advanced or the password you created for API calls.',
            self::SCHEMA_REQUIRED => true,
        ),
    );

    /**
     * Get JS files
     *
     * @return array
     */
    public function getJSFiles()
    {
        $list = parent::getJSFiles();
        $list[] = 'modules/CDev/Paypal/settings/ExpressCheckout/controller.js';

        return $list;
    }

    /**
     * Retrieve property from the request or from  model object
     *
     * @param string $name Field/property name
     *
     * @return mixed
     */
    public function getDefaultFieldValue($name)
    {
        switch ($name) {
            case 'section_merchant_sep':
                $value = 'merchant';
                break;

            case 'section_payflow_sep':
                $value = 'payflow';
                break;

            default:
                $value = parent::getDefaultFieldValue($name);
                break;
        }

        return $value;
    }

    /**
     * Perform some operations when creating fields list by schema
     *
     * @param string $name Node name
     * @param array  $data Field description
     *
     * @return array
     */
    protected function getFieldSchemaArgs($name, array $data)
    {
        $data = parent::getFieldSchemaArgs($name, $data);
        $method = $this->getModelObject();

        switch ($name) {
            case 'section_merchant_sep':
                $processor = $method->getProcessor();
                if (
                    $processor instanceof \XLite\Module\CDev\Paypal\Model\Payment\Processor\ExpressCheckoutMerchantAPI
                    || !$processor->isConfigured($method)
                ) {
                    $value = true;
                } else {
                    $value = false;
                }

                $data[\XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable::PARAM_SELECTED] = $value;
                break;

            case 'section_payflow_sep':
                $processor = $method->getProcessor();
                if (
                    !$processor instanceof \XLite\Module\CDev\Paypal\Model\Payment\Processor\ExpressCheckoutMerchantAPI
                    && $processor->isConfigured($method)
                ) {
                    $value = true;
                } else {
                    $value = false;
                }

                $data[\XLite\Module\CDev\Paypal\View\FormField\Separator\Selectable::PARAM_SELECTED] = $value;
                break;

            default:
                break;
        }

        return $data;
    }

    /**
     * Prepare and save passed data
     *
     * @param array       $data Passed data OPTIONAL
     * @param string|null $name Index in request data array (optional) OPTIONAL
     *
     * @return void
     */
    protected function defineRequestData(array $data = array(), $name = null)
    {
        parent::defineRequestData($data, $name);

        $this->requestData['api_type'] = \XLite\Core\Request::getInstance()->api_type ?: 'merchant';
    }

    /**
     * Check required validation
     *
     * @param array &$data Widget params
     *
     * @return void
     */
    protected function prepareFieldParamsEmail(&$data)
    {
        if ('payflow' == \XLite\Core\Request::getInstance()->api_type) {
            $data[static::SCHEMA_REQUIRED] = false;;
        }
    }

    /**
     * Check required validation
     *
     * @param array &$data Widget params
     *
     * @return void
     */
    protected function prepareFieldParamsPartner(&$data)
    {
        if ('merchant' == \XLite\Core\Request::getInstance()->api_type) {
            $data[static::SCHEMA_REQUIRED] = false;;
        }
    }

    /**
     * Check required validation
     *
     * @param array &$data Widget params
     *
     * @return void
     */
    protected function prepareFieldParamsVendor(&$data)
    {
        if ('merchant' == \XLite\Core\Request::getInstance()->api_type) {
            $data[static::SCHEMA_REQUIRED] = false;;
        }
    }

    /**
     * Check required validation
     *
     * @param array &$data Widget params
     *
     * @return void
     */
    protected function prepareFieldParamsUser(&$data)
    {
        if ('merchant' == \XLite\Core\Request::getInstance()->api_type) {
            $data[static::SCHEMA_REQUIRED] = false;;
        }
    }

    /**
     * Check required validation
     *
     * @param array &$data Widget params
     *
     * @return void
     */
    protected function prepareFieldParamsPwd(&$data)
    {
        if ('merchant' == \XLite\Core\Request::getInstance()->api_type) {
            $data[static::SCHEMA_REQUIRED] = false;;
        }
    }
}
