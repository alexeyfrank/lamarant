<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\CDev\Paypal\Model\Payment\Processor;

/**
 * Paypal Express Checkout payment processor
 */
class ExpressCheckoutMerchantAPI extends \XLite\Module\CDev\Paypal\Model\Payment\Processor\ExpressCheckout
{
    /**
     * Merchant API version
     * https://developer.paypal.com/webapps/developer/docs/classic/release-notes/#MerchantAPI
     */
    const API_VERSION = 115;


    /**
     * API test URL
     *
     * @var string
     */
    protected $apiTestURL = 'https://api-3t.sandbox.paypal.com/nvp';

    /**
     * API live URL
     *
     * @var string
     */
    protected $apiLiveURL = 'https://api-3t.paypal.com/nvp';

    /**
     * Get the list of merchant countries where this payment processor can work
     *
     * @return array
     */
    public function getAllowedMerchantCountries()
    {
        return array('US', 'CA');
    }

    /**
     * Check - payment method is configured or not
     *
     * @param \XLite\Model\Payment\Method $method Payment method
     *
     * @return boolean
     */
    public function isConfigured(\XLite\Model\Payment\Method $method)
    {
        return \XLite\Model\Payment\Base\Processor::isConfigured($method)
            && $method->getSetting('email');
    }

    /**
     * Perform 'SetExpressCheckout' request and get Token value from Paypal
     *
     * @param \XLite\Model\Payment\Method $method Payment method
     *
     * @return string
     */
    public function doSetExpressCheckout(\XLite\Model\Payment\Method $method)
    {
        $token = null;

        if (!isset($this->transaction)) {
            $this->transaction = new \XLite\Model\Payment\Transaction();
            $this->transaction->setPaymentMethod($method);
            $this->transaction->setOrder(\XLite\Model\Cart::getInstance());
        }

        $responseData = $this->doRequest(static::REQ_TYPE_SET_EXPRESS_CHECKOUT);

        if (!empty($responseData['TOKEN'])) {
            $token = $responseData['TOKEN'];

        } elseif (self::EC_TYPE_MARK == \XLite\Core\Session::getInstance()->ec_type) {
            $this->setDetail(
                'status',
                isset($responseData['L_LONGMESSAGE0']) ? $responseData['L_LONGMESSAGE0'] : 'Unknown',
                'Status'
            );

            $this->errorMessage = isset($responseData['L_LONGMESSAGE0']) ? $responseData['L_LONGMESSAGE0'] : null;
        }

        return $token;
    }

    /**
     * doGetExpressCheckoutDetails
     *
     * @param \XLite\Model\Payment\Method $method Payment method object
     *
     * @return array
     */
    public function doGetExpressCheckoutDetails(\XLite\Model\Payment\Method $method)
    {
        $data = array();

        if (!isset($this->transaction)) {
            $this->transaction = new \XLite\Model\Payment\Transaction();
            $this->transaction->setPaymentMethod($method);
        }

        $responseData = $this->doRequest(self::REQ_TYPE_GET_EXPRESS_CHECKOUT_DETAILS);

        if (!empty($responseData) && isset($responseData['ACK']) && 'Success' == $responseData['ACK']) {
            $data = $responseData;
        }

        return $data;
    }

    /**
     * Translate array of data received from Paypal to the array for updating cart
     *
     * @param array $paypalData Array of customer data received from Paypal
     *
     * @return array
     */
    public function prepareBuyerData($paypalData)
    {
        $country = \XLite\Core\Database::getRepo('XLite\Model\Country')
            ->findOneByCode($paypalData['SHIPTOCOUNTRYCODE']);

        $state = \XLite\Core\Database::getRepo('XLite\Model\State')
            ->findOneByCountryAndCode($country->getCode(), $paypalData['SHIPTOSTATE']);

        $data = array(
            'shippingAddress' => array(
                'name' => $paypalData['SHIPTONAME'],
                'street' => $paypalData['SHIPTOSTREET'] . (!empty($paypalData['SHIPTOSTREET2']) ? ' ' . $paypalData['SHIPTOSTREET2'] : ''),
                'country' => $country,
                'state' => $state ? $state : $paypalData['SHIPTOSTATE'],
                'city' => $paypalData['SHIPTOCITY'],
                'zipcode' => $paypalData['SHIPTOZIP'],
                'phone' => isset($paypalData['PHONENUM']) ? $paypalData['PHONENUM'] : '',
            ),
        );

        return $data;
    }


    /**
     * Perform 'DoExpressCheckoutPayment' request and return status of payment transaction
     *
     * @return string
     */
    protected function doExpressCheckoutPayment()
    {
        $status = self::FAILED;

        $transaction = $this->transaction;

        $responseData = $this->doRequest(
            self::REQ_TYPE_DO_EXPRESS_CHECKOUT_PAYMENT,
            $transaction->getInitialBackendTransaction()
        );

        $transactionStatus = $transaction::STATUS_FAILED;

        if (!empty($responseData)) {

            if ('Success' == $responseData['ACK']) {

                if ($this->isSuccessResponse($responseData)) {
                    $transactionStatus = $transaction::STATUS_SUCCESS;
                    $status = self::COMPLETED;

                } else {
                    $transactionStatus = $transaction::STATUS_PENDING;
                    $status = self::PENDING;
                }

            } elseif (
                isset($responseData['L_ERRORCODE0'])
                && '10486' == $responseData['L_ERRORCODE0']
                && $this->isRetryExpressCheckoutAllowed()
            ) {
                $this->retryExpressCheckout(\XLite\Core\Session::getInstance()->ec_token);

            } else {
                $this->setDetail(
                    'status',
                    'Failed: ' . $responseData['L_LONGMESSAGE0'],
                    'Status'
                );
            }

            // Save payment transaction data
            $this->saveFilteredData($responseData);

        } else {
            $this->setDetail(
                'status',
                'Failed: unexpected response received from PayPal',
                'Status'
            );
        }

        $transaction->setStatus($transactionStatus);

        $this->updateInitialBackendTransaction($transaction, $transactionStatus);

        \XLite\Core\Session::getInstance()->ec_token = null;
        \XLite\Core\Session::getInstance()->ec_date = null;
        \XLite\Core\Session::getInstance()->ec_payer_id = null;
        \XLite\Core\Session::getInstance()->ec_type = null;

        return $status;
    }

    /**
     * Get array of common params for all requests
     *
     * @return array
     */
    protected function getCommonRequestParams()
    {
        return array(
            'SUBJECT' => $this->getSetting('email'),
            'VERSION' => static::API_VERSION,
        );
    }

    /**
     * Get array of parameters for SET_EXPRESS_CHECKOUT request
     *
     * @return array
     */
    protected function getSetExpressCheckoutRequestParams()
    {
        $cart = \XLite\Model\Cart::getInstance();

        $amt = $cart->getCurrency()->roundValue($cart->getTotal());

        $shippingModifier = $cart->getModifier(\XLite\Model\Base\Surcharge::TYPE_SHIPPING, 'SHIPPING');
        if ($shippingModifier && $shippingModifier->canApply()) {
            $noShipping = '0';
            $freightAmt = $cart->getCurrency()->roundValue(
                $cart->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_SHIPPING)
            );

        } else {
            $noShipping = '1';
            $freightAmt = 0;
        }

        $postData = array(
            'METHOD'                         => static::REQ_TYPE_SET_EXPRESS_CHECKOUT,
            'RETURNURL'                      => urlencode($this->getECReturnURL()),
            'CANCELURL'                      => urlencode($this->getECReturnURL(true)),
            'NOSHIPPING'                     => $noShipping,
            'ALLOWNOTE'                      => 1,
            'PAYMENTREQUEST_0_INVNUM'        => $this->getSetting('prefix') . $cart->getOrderNumber(),
            'PAYMENTREQUEST_0_CUSTOM'        => $this->getSetting('prefix') . $cart->getOrderNumber(),
            'PAYMENTREQUEST_0_AMT'           => $amt,
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_CURRENCYCODE'  => $cart->getCurrency()->getCode(),
            'PAYMENTREQUEST_0_HANDLINGAMT'   => 0,
            'PAYMENTREQUEST_0_INSURANCEAMT'  => 0,
            'PAYMENTREQUEST_0_SHIPPINGAMT'   => $freightAmt,
        );

        if (\XLite\Core\Config::getInstance()->Security->customer_security) {
            $postData['HDRIMG'] = urlencode(\XLite\Module\CDev\Paypal\Main::getLogo());
        }

        $lineItems = $this->getLineItems($cart);

        // To avoid total mismatch clear tax and shipping cost
        $taxAmt = isset($lineItems['PAYMENTREQUEST_0_TAXAMT']) ? $lineItems['PAYMENTREQUEST_0_TAXAMT'] : 0;
        if (abs($amt - $lineItems['PAYMENTREQUEST_0_ITEMAMT'] - $taxAmt - $freightAmt) > 0.0000000001) {

            $correction = $amt - $lineItems['PAYMENTREQUEST_0_ITEMAMT'] - $taxAmt - $freightAmt;
            $correction = round($correction, 2);

            $index = count($cart->getItems()) + 1;
            $lineItems['L_PAYMENTREQUEST_0_AMT' . $index]  = $correction;
            $lineItems['L_PAYMENTREQUEST_0_NAME' . $index] = 'Correction';
            $lineItems['L_PAYMENTREQUEST_0_QTI' . $index]  = 1;

            $lineItems['PAYMENTREQUEST_0_ITEMAMT'] = $lineItems['PAYMENTREQUEST_0_ITEMAMT'] + $correction;
        }

        $postData += $lineItems;

        $type = \XLite\Core\Session::getInstance()->ec_type;

        if (self::EC_TYPE_SHORTCUT == $type) {
            $postData['REQCONFIRMSHIPPING'] = 0;

        } elseif (self::EC_TYPE_MARK == $type) {
            $postData += array(
                'ADDROVERRIDE'  => 1,
                'PHONENUM'      => $this->getProfile()->getBillingAddress()->getPhone(),
                'EMAIL'         => $this->getProfile()->getLogin(),
            );

            if ('1' !== $noShipping) {
                /** @var \XLite\Model\Address $address */
                $address = $this->getProfile()->getShippingAddress();

                $postData += array(
                    'PAYMENTREQUEST_0_SHIPTONAME'    => trim($address->getFirstname() . ' ' . $address->getLastname()),
                    'PAYMENTREQUEST_0_SHIPTOSTREET'  => $address->getStreet(),
                    'PAYMENTREQUEST_0_SHIPTOSTREET2' => '',
                    'PAYMENTREQUEST_0_SHIPTOCITY'    => $address->getCity(),
                    'PAYMENTREQUEST_0_SHIPTOSTATE'   => $address->getState()->getCode(),
                    'PAYMENTREQUEST_0_SHIPTOZIP'     => $address->getZipcode(),
                    'PAYMENTREQUEST_0_SHIPTOCOUNTRY' => $address->getCountry()->getCode(),
                );
            }
        }

        return $postData;
    }

    /**
     * Get array of params for CREATESECURETOKEN request (ordered products part)
     *
     * @param \XLite\Model\Cart $cart Cart object OPTIONAL
     *
     * @return array
     */
    protected function getLineItems($cart = null)
    {
        $lineItems = array();

        $itemsSubtotal  = 0;
        $itemsTaxAmount = 0;

        $obj = isset($cart) ? $cart : $this->getOrder();

        $items = $obj->getItems();

        if (!empty($items)) {
            $index = 0;

            // Prepare data about ordered products

            foreach ($items as $item) {
                $amt = $obj->getCurrency()->roundValue($item->getItemNetPrice());
                $lineItems['L_PAYMENTREQUEST_0_AMT' . $index] = $amt;

                /** @var \XLite\Model\Product $product */
                $product = $item->getProduct();
                $lineItems['L_PAYMENTREQUEST_0_NAME' . $index] = $product->getTranslation()->name;

                if ($product->getSku()) {
                    $lineItems['L_PAYMENTREQUEST_0_NUMBER' . $index] = $product->getSku();
                }

                $qty = $item->getAmount();
                $lineItems['L_PAYMENTREQUEST_0_QTY' . $index] = $qty;
                $itemsSubtotal += $amt * $qty;
                $index += 1;
            }

            // Prepare data about discount

            $discount = $obj->getCurrency()->roundValue(
                $obj->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_DISCOUNT)
            );

            if (0 != $discount) {
                $lineItems['L_PAYMENTREQUEST_0_AMT' . $index]  = $discount;
                $lineItems['L_PAYMENTREQUEST_0_NAME' . $index] = 'Discount';
                $lineItems['L_PAYMENTREQUEST_0_QTI' . $index]  = 1;
                $itemsSubtotal += $discount;
            }

            $lineItems += array('PAYMENTREQUEST_0_ITEMAMT' => $itemsSubtotal);

            // Prepare data about summary tax cost

            $taxCost = $obj->getCurrency()->roundValue(
                $obj->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_TAX)
            );

            if (0 < $taxCost) {
                $lineItems['L_PAYMENTREQUEST_0_TAXAMT' . $index] = $taxCost;

                $itemsTaxAmount += $taxCost;
                $lineItems['PAYMENTREQUEST_0_TAXAMT'] = $itemsTaxAmount;
            }
        }

        return $lineItems;
    }

    /**
     * Return array of parameters for 'GetExpressCheckoutDetails' request
     *
     * @return array
     */
    protected function getGetExpressCheckoutDetailsRequestParams()
    {
        $params = array(
            'METHOD' => 'GetExpressCheckoutDetails',
            'TOKEN' => \XLite\Core\Session::getInstance()->ec_token,
        );

        return $params;
    }

    /**
     * Return array of parameters for 'DoExpressCheckoutPayment' request
     *
     * @return array
     */
    protected function getDoExpressCheckoutPaymentRequestParams()
    {
        $amt = $this->getOrder()->getCurrency()->roundValue($this->transaction->getValue());
        $freightAmt = $this->getOrder()->getCurrency()->roundValue(
            $this->getOrder()->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_SHIPPING)
        );

        $params = array(
            'METHOD'                         => 'DoExpressCheckoutPayment',
            'TOKEN'                          => \XLite\Core\Session::getInstance()->ec_token,
            'PAYERID'                        => \XLite\Core\Session::getInstance()->ec_payer_id,
            'PAYMENTREQUEST_0_INVNUM'        => $this->getSetting('prefix') . $this->getOrder()->getOrderNumber(),
            'PAYMENTREQUEST_0_CUSTOM'        => $this->getSetting('prefix') . $this->getOrder()->getOrderNumber(),
            'PAYMENTREQUEST_0_AMT'           => $amt,
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_CURRENCYCODE'  => $this->getCurrencyCode(),
            'PAYMENTREQUEST_0_HANDLINGAMT'   => 0,
            'PAYMENTREQUEST_0_INSURANCEAMT'  => 0,
            'PAYMENTREQUEST_0_SHIPPINGAMT'   => $freightAmt,
            'PAYMENTREQUEST_0_NOTIFYURL'     => urlencode($this->getCallbackURL(null, true)),
        );

        $lineItems = $this->getLineItems();

        // To avoid total mismatch clear tax and shipping cost
        $taxAmt = isset($lineItems['PAYMENTREQUEST_0_TAXAMT']) ? $lineItems['PAYMENTREQUEST_0_TAXAMT'] : 0;
        if (abs($amt - $lineItems['PAYMENTREQUEST_0_ITEMAMT'] - $taxAmt - $freightAmt) > 0.0000000001) {

            $correction = $amt - $lineItems['PAYMENTREQUEST_0_ITEMAMT'] - $taxAmt - $freightAmt;
            $correction = round($correction, 2);

            $index = count($this->getOrder()->getItems()) + 1;
            $lineItems['L_PAYMENTREQUEST_0_AMT' . $index]  = $correction;
            $lineItems['L_PAYMENTREQUEST_0_NAME' . $index] = 'Correction';
            $lineItems['L_PAYMENTREQUEST_0_QTI' . $index]  = 1;

            $lineItems['PAYMENTREQUEST_0_ITEMAMT'] = $lineItems['PAYMENTREQUEST_0_ITEMAMT'] + $correction;
        }

        $params += $lineItems;

        return $params;
    }

    /**
     * Get array of params for CREATESCURETOKEN request
     *
     * @param string                                  $requestType Request type
     * @param \XLite\Model\Payment\BackendTransaction $transaction Backend transaction object OPTIONAL
     *
     * @return array
     */
    protected function getRequestParams($requestType, $transaction = null)
    {
        $methodName = 'get' . $requestType . 'RequestParams';

        // Get request params specific for request type
        $postData = $this->$methodName($transaction) + $this->getCommonRequestParams();

        $data = array();

        foreach ($postData as $k => $v) {
            $data[] = sprintf('%s=%s', $k, $v);
        }

        $data = implode('&', $data);

        return $data;
    }

    /**
     * Return true if Paypal response is a success transaction response
     *
     * @param array $response Response data
     *
     * @return boolean
     */
    protected function isSuccessResponse($response)
    {
        $result = in_array($response['PAYMENTINFO_0_PENDINGREASON'], array('none', 'completed'));

        if (!$result) {
            $result = (
                'authorization' == $response['PAYMENTINFO_0_PENDINGREASON']
                && \XLite\Model\Payment\BackendTransaction::TRAN_TYPE_AUTH == $this->transaction->getType()
            );
        }

        return $result;
    }

    /**
     * Parse response from Paypal and return result as an array
     * e.g. the response "RESULT=0&SECURETOKEN=3DbhdANpkkkOZ8byxZtaRCQQ7&SECURETOKENID=82248f5c934f88466ab95965118f5ef1&RESPMSG=Approved"
     * will return an array:
     * array(
     *   "RESULT"        => "0",
     *   "SECURETOKEN"   => "3DbhdANpkkkOZ8byxZtaRCQQ7",
     *   "SECURETOKENID" => "82248f5c934f88466ab95965118f5ef1",
     *   "RESPMSG"       => "Approved",
     * );
     *
     * @param string $response Response
     *
     * @return array
     */
    protected function getParsedResponse($response)
    {
        $result = array();

        $rows = explode('&', $response);

        if (is_array($rows)) {
            foreach ($rows as $row) {
                list($key, $value) = explode('=', $row);
                $result[$key] = urldecode($value);
            }
        }

        return $result;
    }

    /**
     * Get allowed currencies
     * https://developer.paypal.com/docs/classic/api/currency_codes/
     *
     * @param \XLite\Model\Payment\Method $method Payment method
     *
     * @return array
     */
    protected function getAllowedCurrencies(\XLite\Model\Payment\Method $method)
    {
        return array_merge(
            parent::getAllowedCurrencies($method),
            array(
                'AUD', 'BRL', 'CAD', 'CZK', 'DKK',
                'EUR', 'HKD', 'HUF', 'ILS', 'JPY',
                'MYR', 'MXN', 'NOK', 'NZD', 'PHP',
                'PLN', 'GBP', 'RUB', 'SGD', 'SEK',
                'CHF', 'TWD', 'THB', 'TRY', 'USD',
            )
        );
    }
}
