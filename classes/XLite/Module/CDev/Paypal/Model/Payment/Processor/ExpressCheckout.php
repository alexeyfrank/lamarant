<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\CDev\Paypal\Model\Payment\Processor;

/**
 * Paypal Express Checkout payment processor
 */
class ExpressCheckout extends \XLite\Module\CDev\Paypal\Model\Payment\Processor\APaypal
{
    /**
     * Request types definition
     */
    const REQ_TYPE_SET_EXPRESS_CHECKOUT         = 'SetExpressCheckout';
    const REQ_TYPE_GET_EXPRESS_CHECKOUT_DETAILS = 'GetExpressCheckoutDetails';
    const REQ_TYPE_DO_EXPRESS_CHECKOUT_PAYMENT  = 'DoExpressCheckoutPayment';

    /**
     * Express Checkout flow types definition
     */
    const EC_TYPE_SHORTCUT = 'shortcut';
    const EC_TYPE_MARK     = 'mark';

    /**
     * Express Checkout token TTL is 3 hours (10800 seconds)
     */
    const TOKEN_TTL = 10800;

    /**
     * Maximum tries to checkout when getting 10486 error
     */
    const MAX_TRIES = 3;

    /**
     * Live PostURL
     *
     * @var string
     */
    protected $livePostURL = 'https://www.paypal.com/cgi-bin/webscr';

    /**
     * Test PostURL
     *
     * @var string
     */
    protected $testPostURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

    /**
     * Referral page URL
     *
     * @var string
     */
    protected $referralPageURL = 'https://www.paypal.com/webapps/mpp/referral/paypal-express-checkout?partner_id=';

    /**
     * Knowledge base page URL
     *
     * @var string
     */
    protected $knowledgeBasePageURL = 'http://kb.x-cart.com/pages/viewpage.action?pageId=7505722';

    /**
     * Error message
     *
     * @var string
     */
    protected $errorMessage = null;

    /**
     * Get payment method row checkout template
     *
     * @param \XLite\Model\Payment\Method $method Payment method
     *
     * @return string
     */
    public function getCheckoutTemplate(\XLite\Model\Payment\Method $method)
    {
        return 'modules/CDev/Paypal/checkout/expressCheckout.tpl';
    }

    /**
     * Get the list of merchant countries where this payment processor can work
     *
     * @return array
     */
    public function getAllowedMerchantCountries()
    {
        return array('US', 'CA');
    }

    /**
     * Perform 'SetExpressCheckout' request and get Token value from Paypal
     *
     * @param \XLite\Model\Payment\Method $method Payment method
     *
     * @return string
     */
    public function doSetExpressCheckout(\XLite\Model\Payment\Method $method)
    {
        $token = null;

        if (!isset($this->transaction)) {
            $this->transaction = new \XLite\Model\Payment\Transaction();
            $this->transaction->setPaymentMethod($method);
            $this->transaction->setOrder(\XLite\Model\Cart::getInstance());
        }

        $responseData = $this->doRequest(self::REQ_TYPE_SET_EXPRESS_CHECKOUT);

        if (!empty($responseData['TOKEN'])) {
            $token = $responseData['TOKEN'];

        } elseif (self::EC_TYPE_MARK == \XLite\Core\Session::getInstance()->ec_type) {
            $this->setDetail(
                'status',
                isset($responseData['RESPMSG']) ? $responseData['RESPMSG'] : 'Unknown',
                'Status'
            );

            $this->errorMessage = isset($responseData['RESPMSG']) ? $responseData['RESPMSG'] : null;
        }

        return $token;
    }

    /**
     * Redirect customer to Paypal server for authorization and address selection
     *
     * @param string $token Express Checkout token
     *
     * @return void
     */
    public function redirectToPaypal($token)
    {
        $url = $this->getRedirectURL($this->getPostParams($token));

        \XLite\Module\CDev\Paypal\Main::addLog(
            'redirectToPaypal()',
            $url
        );

        $page = <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body onload="javascript: self.location = '$url';">
</body>
</html>
HTML;

        print ($page);
    }

    /**
     * doGetExpressCheckoutDetails
     *
     * @param \XLite\Model\Payment\Method $method Payment method object
     *
     * @return array
     */
    public function doGetExpressCheckoutDetails(\XLite\Model\Payment\Method $method)
    {
        $data = array();

        if (!isset($this->transaction)) {
            $this->transaction = new \XLite\Model\Payment\Transaction();
            $this->transaction->setPaymentMethod($method);
        }

        $responseData = $this->doRequest(self::REQ_TYPE_GET_EXPRESS_CHECKOUT_DETAILS);

        if (!empty($responseData) && '0' == $responseData['RESULT']) {
            $data = $responseData;
        }

        return $data;
    }

    /**
     * Process return (this used when customer pay via Express Checkout mark flow)
     *
     * @param \XLite\Model\Payment\Transaction $transaction Payment transaction object
     *
     * @return void
     */
    public function processReturn(\XLite\Model\Payment\Transaction $transaction)
    {
        parent::processReturn($transaction);

        if (!\XLite\Core\Request::getInstance()->cancel) {
            \XLite\Core\Session::getInstance()->ec_payer_id = \XLite\Core\Request::getInstance()->PayerID;
            $this->doExpressCheckoutPayment();
        }
    }

    /**
     * Translate array of data received from Paypal to the array for updating cart
     *
     * @param array $paypalData Array of customer data received from Paypal
     *
     * @return array
     */
    public function prepareBuyerData($paypalData)
    {
        $country = \XLite\Core\Database::getRepo('XLite\Model\Country')
            ->findOneByCode($paypalData['SHIPTOCOUNTRY']);

        $state = \XLite\Core\Database::getRepo('XLite\Model\State')
            ->findOneByCountryAndCode($country->getCode(), $paypalData['SHIPTOSTATE']);

        $data = array(
            'shippingAddress' => array(
                'name' => $paypalData['SHIPTONAME'],
                'street' => $paypalData['SHIPTOSTREET'] . (!empty($paypalData['SHIPTOSTREET2']) ? ' ' . $paypalData['SHIPTOSTREET2'] : ''),
                'country' => $country,
                'state' => $state ? $state : $paypalData['SHIPTOSTATE'],
                'city' => $paypalData['SHIPTOCITY'],
                'zipcode' => $paypalData['SHIPTOZIP'],
                'phone' => isset($paypalData['PHONENUM']) ? $paypalData['PHONENUM'] : '',
            ),
        );

        return $data;
    }

    /**
     * Get PostURL to redirect customer to Paypal
     *
     * @return string
     */
    protected function getExpressCheckoutPostURL()
    {
        return $this->isTestMode($this->transaction->getPaymentMethod()) ? $this->testPostURL : $this->livePostURL;
    }

    /**
     * Get array of parameters for redirecting customer to Paypal server
     *
     * @param string $token Express Checkout token
     *
     * @return array
     */
    protected function getPostParams($token)
    {
        $params = array(
            'cmd' => '_express-checkout',
            'token' => $token,
        );

        if (self::EC_TYPE_MARK == \XLite\Core\Session::getInstance()->ec_type) {
            $params['useraction'] = 'commit';
        }

        return $params;
    }

    /**
     * Get array of parameters for SET_EXPRESS_CHECKOUT request
     *
     * @return array
     */
    protected function getSetExpressCheckoutRequestParams()
    {
        $cart = \XLite\Model\Cart::getInstance();

        $amt = $cart->getCurrency()->roundValue($cart->getTotal());

        $shippingModifier = $cart->getModifier(\XLite\Model\Base\Surcharge::TYPE_SHIPPING, 'SHIPPING');
        if ($shippingModifier && $shippingModifier->canApply()) {
            $noShipping = '0';
            $freightAmt = $cart->getCurrency()->roundValue(
                $cart->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_SHIPPING)
            );

        } else {
            $noShipping = '1';
            $freightAmt = 0;
        }

        $postData = array(
            'TRXTYPE'           => $this->getSetting('transaction_type'),
            'TENDER'            => 'P',
            'ACTION'            => 'S',
            'RETURNURL'         => urldecode($this->getECReturnURL()),
            'CANCELURL'         => urldecode($this->getECReturnURL(true)),
            'AMT'               => $amt,
            'CURRENCY'          => $cart->getCurrency()->getCode(),
            'FREIGHTAMT'        => $freightAmt,
            'HANDLINGAMT'       => 0,
            'INSURANCEAMT'      => 0,
            'NOSHIPPING'        => $noShipping,
            'INVNUM'            => $this->getSetting('prefix') . $cart->getOrderNumber(),
            'ALLOWNOTE'         => 1,
            'CUSTOM'            => $this->getSetting('prefix') . $cart->getOrderNumber(),
        );

        if (\XLite\Core\Config::getInstance()->Security->customer_security) {
            $postData['HDRIMG'] = urldecode(\XLite\Module\CDev\Paypal\Main::getLogo());
        }

        $lineItems = $this->getLineItems($cart);

        // To avoid total mismatch clear tax and shipping cost
        $taxAmt = isset($lineItems['TAXAMT']) ? $lineItems['TAXAMT'] : 0;
        if (abs($amt - $lineItems['ITEMAMT'] - $taxAmt - $freightAmt) > 0.0000000001) {
            $lineItems['ITEMAMT'] = $amt;
            $lineItems['TAXAMT'] = 0;
            $postData['FREIGHTAMT'] = 0;
        }

        $postData += $lineItems;

        $type = \XLite\Core\Session::getInstance()->ec_type;

        if (self::EC_TYPE_SHORTCUT == $type) {
            $postData['REQCONFIRMSHIPPING'] = 0;

        } elseif (self::EC_TYPE_MARK == $type) {
            $postData += array(
                'ADDROVERRIDE'  => 1,
                'PHONENUM'      => $this->getProfile()->getBillingAddress()->getPhone(),
                'EMAIL'         => $this->getProfile()->getLogin(),
            );

            if ('1' !== $noShipping) {
                /** @var \XLite\Model\Address $address */
                $address = $this->getProfile()->getShippingAddress();

                $postData += array(
                    'SHIPTONAME'    => trim($address->getFirstname() . ' ' . $address->getLastname()),
                    'SHIPTOSTREET'  => $address->getStreet(),
                    'SHIPTOSTREET2' => '',
                    'SHIPTOCITY'    => $address->getCity(),
                    'SHIPTOSTATE'   => $address->getState()->getCode(),
                    'SHIPTOZIP'     => $address->getZipcode(),
                    'SHIPTOCOUNTRY' => $address->getCountry()->getCode(),
                );
            }
        }

        return $postData;
    }


    /**
     * Return array of parameters for 'GetExpressCheckoutDetails' request
     *
     * @return array
     */
    protected function getGetExpressCheckoutDetailsRequestParams()
    {
        $params = array(
            'TRXTYPE' => $this->getSetting('transaction_type'),
            'TENDER' => 'P',
            'ACTION' => 'G',
            'TOKEN' => \XLite\Core\Session::getInstance()->ec_token,
        );

        return $params;
    }


    /**
     * Do initial payment and return status
     *
     * @return string
     */
    protected function doInitialPayment()
    {
        $this->transaction->createBackendTransaction($this->getInitialTransactionType());

        $result = self::FAILED;

        if (!$this->isTokenValid() || self::EC_TYPE_MARK == \XLite\Core\Session::getInstance()->ec_type) {

            \XLite\Core\Session::getInstance()->ec_type = self::EC_TYPE_MARK;

            $token = $this->doSetExpressCheckout($this->transaction->getPaymentMethod());

            if (isset($token)) {
                \XLite\Core\Session::getInstance()->ec_token = $token;
                \XLite\Core\Session::getInstance()->ec_date = \XLite\Core\Converter::time();
                \XLite\Core\Session::getInstance()->ec_payer_id = null;

                $result = static::PROLONGATION;

                $this->redirectToPaypal($token);

                if (self::EC_TYPE_MARK !== \XLite\Core\Session::getInstance()->ec_type) {
                    exit ();
                }

            } else {
                $this->transaction->setDataCell(
                    'status',
                    $this->errorMessage ?: 'Failure to redirect to PayPal.',
                    null,
                    'C'
                );
            }

        } else {
            $result = $this->doExpressCheckoutPayment();
        }

        return $result;
    }

    /**
     * Returns true if token initialized and is not expired
     *
     * @return boolean
     */
    protected function isTokenValid()
    {
        return !empty(\XLite\Core\Session::getInstance()->ec_token)
            && self::TOKEN_TTL > \XLite\Core\Converter::time() - \XLite\Core\Session::getInstance()->ec_date;
    }

    /**
     * Perform 'DoExpressCheckoutPayment' request and return status of payment transaction
     *
     * @return string
     */
    protected function doExpressCheckoutPayment()
    {
        $status = self::FAILED;

        $transaction = $this->transaction;

        $responseData = $this->doRequest(
            self::REQ_TYPE_DO_EXPRESS_CHECKOUT_PAYMENT,
            $transaction->getInitialBackendTransaction()
        );

        $transactionStatus = $transaction::STATUS_FAILED;

        if (!empty($responseData)) {

            if ('0' == $responseData['RESULT']) {

                if ($this->isSuccessResponse($responseData)) {
                    $transactionStatus = $transaction::STATUS_SUCCESS;
                    $status = self::COMPLETED;

                } else {
                    $transactionStatus = $transaction::STATUS_PENDING;
                    $status = self::PENDING;
                }

            } elseif (
                (
                    preg_match('/^Generic processor error: 10486/', $responseData['RESPMSG'])
                    || preg_match('/^10486/', $responseData['RESPMSG'])
                )
                && $this->isRetryExpressCheckoutAllowed()
            ) {
                $this->retryExpressCheckout(\XLite\Core\Session::getInstance()->ec_token);

            } else {
                $this->setDetail(
                    'status',
                    'Failed: ' . $responseData['RESPMSG'],
                    'Status'
                );
            }

            // Save payment transaction data
            $this->saveFilteredData($responseData);

        } else {
            $this->setDetail(
                'status',
                'Failed: unexpected response received from PayPal',
                'Status'
            );
        }

        $transaction->setStatus($transactionStatus);

        $this->updateInitialBackendTransaction($transaction, $transactionStatus);

        \XLite\Core\Session::getInstance()->ec_token = null;
        \XLite\Core\Session::getInstance()->ec_date = null;
        \XLite\Core\Session::getInstance()->ec_payer_id = null;
        \XLite\Core\Session::getInstance()->ec_type = null;

        return $status;
    }

    /**
     * Retry ExpressCheckout
     *
     * @param string $token Express Checkout token value
     *
     * @return void
     */
    protected function retryExpressCheckout($token)
    {
        \XLite\Core\Session::getInstance()->expressCheckoutRetry
            = (\XLite\Core\Session::getInstance()->expressCheckoutRetry ?: 0) + 1;

        $this->redirectToPaypal($token);

        exit ();
    }

    /**
     * Is retryExpressCheckout allowed
     *
     * @return boolean
     */
    protected function isRetryExpressCheckoutAllowed()
    {
        $result = is_null(\XLite\Core\Session::getInstance()->expressCheckoutRetry)
            || \XLite\Core\Session::getInstance()->expressCheckoutRetry < static::MAX_TRIES;

        if (!$result) {
            \XLite\Core\Session::getInstance()->expressCheckoutRetry = 0;
        }

        return $result;
    }

    /**
     * Return array of parameters for 'DoExpressCheckoutPayment' request
     *
     * @return array
     */
    protected function getDoExpressCheckoutPaymentRequestParams()
    {
        $amt = $this->getOrder()->getCurrency()->roundValue($this->transaction->getValue());

        $freightAmt = $this->getOrder()->getCurrency()->roundValue(
            $this->getOrder()->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_SHIPPING)
        );

        $params = array(
            'TRXTYPE'      => $this->getSetting('transaction_type'),
            'TENDER'       => 'P',
            'ACTION'       => 'D',
            'TOKEN'        => \XLite\Core\Session::getInstance()->ec_token,
            'PAYERID'      => \XLite\Core\Session::getInstance()->ec_payer_id,
            'AMT'          => $amt,
            'CURRENCY'     => $this->getCurrencyCode(),
            'FREIGHTAMT'   => $freightAmt,
            'HANDLINGAMT'  => 0,
            'INSURANCEAMT' => 0,
            'NOTIFYURL'    => $this->getCallbackURL(null, true),
            'INVNUM'       => $this->getSetting('prefix') . $this->getOrder()->getOrderNumber(),
            'ALLOWNOTE'    => 1,
            'CUSTOM'       => $this->getSetting('prefix') . $this->getOrder()->getOrderNumber(),
        );

        $lineItems = $this->getLineItems();

        // To avoid total mismatch clear tax and shipping cost
        $taxAmt = isset($lineItems['TAXAMT']) ? $lineItems['TAXAMT'] : 0;
        if (abs($amt - $lineItems['ITEMAMT'] - $taxAmt - $freightAmt) > 0.0000000001) {
            $lineItems['ITEMAMT'] = $amt;
            $lineItems['TAXAMT'] = 0;
            $params['FREIGHTAMT'] = 0;
        }

        $params += $lineItems;

        return $params;
    }

    /**
     * Get return URL
     *
     * @param boolean $asCancel Flag: true if URL is for Cancel action) OPTIONAL
     *
     * @return string
     */
    protected function getECReturnURL($asCancel = false)
    {
        $params = $asCancel ? array('cancel' => 1) : array();

        if (self::EC_TYPE_MARK == \XLite\Core\Session::getInstance()->ec_type) {
            $url = $this->getReturnURL(null, true, $asCancel);
        } else {
            $url = \XLite::getInstance()->getShopURL(
                \XLite\Core\Converter::buildURL('checkout', 'express_checkout_return', $params),
                \XLite\Core\Config::getInstance()->Security->customer_security
            );
        }

        return $url;
    }

    /**
     * Get allowed currencies
     * https://www.paypalobjects.com/webstatic/en_US/developer/docs/pdf/pfp_expresscheckout_pp.pdf
     *
     * @param \XLite\Model\Payment\Method $method Payment method
     *
     * @return array
     */
    protected function getAllowedCurrencies(\XLite\Model\Payment\Method $method)
    {
        return array_merge(
            parent::getAllowedCurrencies($method),
            array(
                'USD', 'CAD', 'EUR', 'GBP', 'AUD',
                'CHF', 'JPY', 'NOK', 'NZD', 'PLN',
                'SEK', 'SGD', 'HKD', 'DKK', 'HUF',
                'CZK', 'BRL', 'ILS', 'MYR', 'MXN',
                'PHP', 'TWD', 'THB',
            )
        );
    }

    /**
     * Get post URL
     *
     * @param array $params Array of URL parameters OPTIONAL
     *
     * @return string
     */
    protected function getRedirectURL($params = array())
    {
        $postURL = $this->getExpressCheckoutPostURL();

        $postData = array();

        foreach ($params as $k => $v) {
            $postData[] = sprintf('%s=%s', $k, $v);
        }

        return $postURL . '?' . implode('&', $postData);
    }
}
