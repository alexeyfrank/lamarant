<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\CDev\Paypal\Controller\Admin;

/**
 * Paypal settings controller
 */
class PaypalSettings extends \XLite\Controller\Admin\AAdmin
{
    /**
     * Paypal module string name for payment methods
     */
    const MODULE_NAME = 'CDev_Paypal';

    /**
     * Return the current page title (for the content area)
     *
     * @return string
     */
    public function getTitle()
    {
        return null;
    }

    /**
     * Get method id from request
     *
     * @return integer
     */
    public function getMethodId()
    {
        return \XLite\Core\Request::getInstance()->method_id;
    }

    /**
     * Get payment method
     *
     * @return \XLite\Model\Payment\Method
     */
    public function getPaymentMethod()
    {
        $paymentMethod = $this->getMethodId()
            ? \XLite\Core\Database::getRepo('\XLite\Model\Payment\Method')->find($this->getMethodId())
            : null;

        return $paymentMethod && static::MODULE_NAME === $paymentMethod->getModuleName()
            ? $paymentMethod
            : null;
    }

    /**
     * Do action 'Update'
     *
     * @return void
     */
    protected function doActionUpdate()
    {
        $this->getModelForm()->performAction('modify');
    }

    /**
     * Return class name for the controller main form
     *
     * @return string
     */
    protected function getModelFormClass()
    {
        $namespace = '\XLite\Module\CDev\Paypal\View\Model';
        $className = $this->getPaymentMethod()->getServiceName();

        return $namespace . '\\' . $className;
    }

    /**
     * Hide payment settings instruction block
     *
     * @return void
     */
    protected function doActionHideInstruction()
    {
        $this->switchDisplayInstructionFlag(true);
    }

    /**
     * Hide payment settings instruction block
     *
     * @return void
     */
    protected function doActionShowInstruction()
    {
        $this->switchDisplayInstructionFlag(false);
    }

    /**
     * Switch hide_instruction parameter
     *
     * @param boolean $value Value of parameter
     *
     * @return void
     */
    protected function switchDisplayInstructionFlag($value)
    {
        $paymentMethod = $this->getPaymentMethod();

        if ($paymentMethod) {
            $paymentMethod->setSetting('hide_instruction', $value);
            \XLite\Core\Database::getRepo('\XLite\Model\Payment\Method')->update($paymentMethod);
        }

        $this->setReturnURL(
            $this->buildURL(
                'paypal_settings',
                null,
                array('method_id' => $this->getMethodId())
            )
        );
    }
}
