<?php

namespace XLite\Module\Frank\CallOrders\Model;

class Product extends \XLite\Model\Product implements \XLite\Base\IDecorator {
    /**
     * Product old price
     *
     * @var string
     *
     * @Column (type="string", length=32, nullable=true)
     */
    protected $old_price;

    /**
     * Product badges
     *
     * @var string
     *
     * @Column (type="string", length=255, nullable=true)
     */
    protected $badges;


    public function getBadgesArray() {
        if (!$this->getBadges()) {
            return array();
        }

        return array_filter(explode(" ", $this->getBadges()));
    }

    public function getProductDiscount() {
        return intval($this->getProductSaving() / intval($this->old_price) * 100);
    }

    public function getProductSaving() {
        return intval($this->old_price) - intval($this->price);
    }
}

