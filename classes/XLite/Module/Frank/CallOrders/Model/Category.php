<?php

namespace XLite\Module\Frank\CallOrders\Model;

class Category extends \XLite\Model\Category implements \XLite\Base\IDecorator
{
    /**
     * @var string
     *
     * @Column (type="string", length=255, nullable=true)
     */
    protected $menu_subtitle;

    /**
     * One-to-one relation with category_images table
     *
     * @OneToOne  (targetEntity="XLite\Module\Frank\CallOrders\Model\Image\Category\MenuImage", mappedBy="category", cascade={"all"})
     */
    protected $menu_image;
}
