<?php

namespace XLite\Module\Frank\CallOrders\Model\Image\Category;

/**
 * Category
 *
 * @Entity
 * @Table  (name="category_menu_images")
 */
class MenuImage extends \XLite\Model\Base\Image
{
   /**
    * @Id
    * @GeneratedValue (strategy="AUTO")
    * @Column         (type="integer")
    */
   protected $id;

    /**
     * Relation to a category/menu_image entity
     *
     * @var \XLite\Module\Frank\CallOrders\Model\Image\Category\MenuImage
     *
     * @OneToOne   (targetEntity="XLite\Model\Category", inversedBy="menu_image")
     * @JoinColumn (referencedColumnName="category_id")
     */
    protected $category;

    /**
     * Alternative image text
     *
     * @var string
     *
     * @Column (type="string", length=255)
     */
    protected $alt = '';
}

