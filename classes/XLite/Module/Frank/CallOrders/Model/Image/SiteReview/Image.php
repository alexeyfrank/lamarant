<?php

namespace XLite\Module\Frank\CallOrders\Model\Image\SiteReview;

/**
 * Category
 *
 * @Entity
 * @Table  (name="site_review_images")
 */
class Image extends \XLite\Model\Base\Image
{
    /**
     * Relation to a site_review entity
     *
     * @var \XLite\Module\Frank\CallOrders\Model\SiteReview
     *
     * @OneToOne   (targetEntity="XLite\Module\Frank\CallOrders\Model\SiteReview", inversedBy="image")
     * @JoinColumn (name="site_review_id", referencedColumnName="id")
     */
    protected $site_review;

    /**
     * Alternative image text
     *
     * @var string
     *
     * @Column (type="string", length=255)
     */
    protected $alt = '';
}
