<?php
namespace XLite\Module\Frank\CallOrders\Model\Repo\Image\SiteReview;

/**
 * Category
 */
class Image extends \XLite\Model\Repo\Base\Image
{
    /**
     * Returns the name of the directory within 'root/images' where images stored
     *
     * @return string
     */
    public function getStorageName()
    {
        return 'site_review';
    }
}
