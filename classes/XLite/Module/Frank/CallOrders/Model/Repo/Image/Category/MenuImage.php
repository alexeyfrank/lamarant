<?php
namespace XLite\Module\Frank\CallOrders\Model\Repo\Image\Category;

/**
 * Category
 */
class MenuImage extends \XLite\Model\Repo\Base\Image
{
    /**
     * Returns the name of the directory within 'root/images' where images stored
     *
     * @return string
     */
    public function getStorageName()
    {
        return 'category_menu_image';
    }
}
