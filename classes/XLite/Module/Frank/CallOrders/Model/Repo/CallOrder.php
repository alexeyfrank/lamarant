<?php

namespace XLite\Module\Frank\CallOrders\Model\Repo;

class CallOrder extends \XLite\Model\Repo\ARepo
{
   // {{{ Search


   const SEARCH_LIMIT = 'limit';


   public function search(\XLite\Core\CommonCell $cnd, $countOnly = false)
   {
       $queryBuilder = $this->createQueryBuilder('n');
       $this->currentSearchCnd = $cnd;


       foreach ($this->currentSearchCnd as $key => $value) {
           $this->callSearchConditionHandler($value, $key, $queryBuilder, $countOnly);
       }


       return $countOnly
           ? $this->searchCount($queryBuilder)
           : $this->searchResult($queryBuilder);
   }


   public function searchCount(\Doctrine\ORM\QueryBuilder $qb)
   {
       $qb->select('COUNT(DISTINCT n.id)');


       return intval($qb->getSingleScalarResult());
   }


   public function searchResult(\Doctrine\ORM\QueryBuilder $qb)
   {
       return $qb->getResult();
   }


   protected function callSearchConditionHandler($value, $key, \Doctrine\ORM\QueryBuilder $queryBuilder, $countOnly)
   {
       if ($this->isSearchParamHasHandler($key)) {
           $this->{'prepareCnd' . ucfirst($key)}($queryBuilder, $value, $countOnly);
       }
   }


   protected function isSearchParamHasHandler($param)
   {
       return in_array($param, $this->getHandlingSearchParams());
   }


   protected function getHandlingSearchParams()
   {
       return array(
           static::SEARCH_LIMIT,
       );
   }

    /**
     * Prepare certain search condition
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder Query builder to prepare
     * @param array                      $value        Condition data
     * @param boolean                    $countOnly    "Count only" flag. Do not need to add "order by" clauses if only count is needed.
     *
     * @return void
     */
    protected function prepareCndOrderBy(\Doctrine\ORM\QueryBuilder $queryBuilder, array $value, $countOnly)
    {
        if (!$countOnly) {
            list($sort, $order) = $this->getSortOrderValue($value);
            $queryBuilder->addOrderBy($sort, $order);
        }
    }

    /**
     * Prepare certain search condition
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder Query builder to prepare
     * @param array                      $value        Condition data
     *
     * @return void
     */
    protected function prepareCndLimit(\Doctrine\ORM\QueryBuilder $queryBuilder, array $value)
    {
        $queryBuilder->setFrameResults($value);
    }
}
