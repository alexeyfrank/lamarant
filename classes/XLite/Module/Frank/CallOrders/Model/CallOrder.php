<?php

namespace XLite\Module\Frank\CallOrders\Model;

/**
 * @Entity
 * @Table  (name="call_orders")
 */
class CallOrder extends \XLite\Model\AEntity
{
   /**
    * @Id
    * @GeneratedValue (strategy="AUTO")
    * @Column         (type="integer")
    */
   protected $id;


   /**
    * @Column (type="boolean")
    */
   protected $processed = false;


   /**
    * @Column (type="string", length=255)
    */
   protected $name = '';


   /**
    * @Column (type="string", length=255)
    */
   protected $phone = '';
}
