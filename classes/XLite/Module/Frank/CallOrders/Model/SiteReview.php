<?php

namespace XLite\Module\Frank\CallOrders\Model;

/**
 * @Entity
 * @Table  (name="site_reviews")
 * @HasLifeCycleCallbacks
 */
class SiteReview extends \XLite\Model\AEntity
{
    /**
     * @Id
     * @GeneratedValue (strategy="AUTO")
     * @Column         (type="integer")
     */
    protected $id;

    /**
     * @Column (type="boolean")
     */
    protected $published = false;

    /**
     * One-to-one relation with category_images table
     *
     * @var \XLite\Model\Image\Category\Image
     *
     * @OneToOne  (targetEntity="XLite\Module\Frank\CallOrders\Model\Image\SiteReview\Image", mappedBy="site_review", cascade={"all"})
     */
    protected $image;


    /**
     * @Column (type="string", length=255)
     */
    protected $name = '';

    /**
     * @Column (type="string", length=255)
     */
    protected $city = '';


    /**
     * @Column (type="text")
     */
    protected $text = '';

    /**
     * Creation date (UNIX Timestamp)
     * @var integer
     * @Column (type="integer")
     */
    protected $date = 0;

    /**
     * @return void
     * @PrePersist
     */
    public function prepareBeforeCreate() {
        $time = \XLite\Core\Converter::time();
        if (!$this->getDate()) {
            $this->setDate($time);
        }
    }
}
