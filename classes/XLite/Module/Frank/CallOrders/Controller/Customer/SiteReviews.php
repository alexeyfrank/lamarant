<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\Frank\CallOrders\Controller\Customer;

class SiteReviews extends \XLite\Controller\Customer\ACustomer
{
    protected function getLocation() {
        return "Отзывы";
    }

    protected function getRepo() {
        return \XLite\Core\Database::getRepo('\XLite\Module\Frank\CallOrders\Model\SiteReview');
    }
    public function getSiteReviews()
    {
        return $this->getRepo()->getPublishedOrderByDesc()->getResult();
    }

    protected function doActionCreate()
    {
        $data = \XLite\Core\Request::getInstance()->getData();
        $model = new \XLite\Module\Frank\CallOrders\Model\SiteReview();
        $model->map($data);

        \XLite\Core\Database::getEM()->persist($model);
        \XLite\Core\Database::getEM()->flush();

        return $model;
    }
}
