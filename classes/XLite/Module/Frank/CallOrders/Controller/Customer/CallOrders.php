<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\Frank\CallOrders\Controller\Customer;


class CallOrders extends \XLite\Controller\Customer\ACustomer
{
    protected function doActionCreate()
    {
        $data = \XLite\Core\Request::getInstance()->getData();
        $callOrder = new \XLite\Module\Frank\CallOrders\Model\CallOrder();
        $callOrder->map($data);

        \XLite\Core\Database::getEM()->persist($callOrder);

        \XLite\Core\Database::getEM()->flush();

        return $callOrder;
    }
}

