<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\Frank\CallOrders\Controller\Customer;

class Lookbook extends \XLite\Controller\Customer\ACustomer
{
    protected function getLocation() {
        return "Look Book";
    }
}
