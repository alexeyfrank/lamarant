<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\Frank\CallOrders\Controller\Admin;

/**
 * Call orders controller
 */
class SiteReviews extends \XLite\Controller\Admin\AAdmin
{
    protected function doActionUpdate()
    {
        $list = new \XLite\Module\Frank\CallOrders\View\ItemsList\Model\SiteReview;
        $list->processQuick();
    }
}
