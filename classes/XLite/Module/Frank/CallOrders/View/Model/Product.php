<?php
namespace XLite\Module\Frank\CallOrders\View\Model;

/**
 * Product view model
 */
class Product extends \XLite\View\Model\Product implements \XLite\Base\IDecorator
{
    protected $schemaDefault = array(
        'sku' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text\SKU',
            self::SCHEMA_LABEL    => 'SKU',
            self::SCHEMA_REQUIRED => false,
        ),
        'name' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Product Name',
            self::SCHEMA_REQUIRED => true,
        ),
        'badges' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Product Badges',
            self::SCHEMA_REQUIRED => false,
        ),
        'categories' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Select\Categories',
            self::SCHEMA_LABEL    => 'Category',
            self::SCHEMA_REQUIRED => false,
        ),
        'images' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\FileUploader\Image',
            self::SCHEMA_LABEL    => 'Images',
            self::SCHEMA_REQUIRED => false,
            \XLite\View\FormField\FileUploader\Image::PARAM_MULTIPLE => true,
        ),
        'memberships' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Select\Memberships',
            self::SCHEMA_LABEL    => 'Memberships',
            self::SCHEMA_REQUIRED => false,
            self::SCHEMA_COMMENT  => 'Do not select anything if you want to make the product visible to all customers.',
        ),
        'taxClass' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Select\TaxClass',
            self::SCHEMA_LABEL    => 'Tax class',
            self::SCHEMA_REQUIRED => false,
        ),
        'price' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text\Price',
            self::SCHEMA_LABEL    => 'Price',
            self::SCHEMA_REQUIRED => false,
        ),
        'old_price' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Old price',
            self::SCHEMA_REQUIRED => false,
        ),
        'qty' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text\ProductQuantity',
            self::SCHEMA_LABEL    => 'Quantity in stock',
            self::SCHEMA_REQUIRED => false,
        ),
        'weight' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text\Weight',
            self::SCHEMA_LABEL    => 'Weight',
            self::SCHEMA_REQUIRED => false,
        ),
        'shippable' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Select\YesNo',
            self::SCHEMA_LABEL    => 'Shippable',
            self::SCHEMA_REQUIRED => false,
        ),
        'useSeparateBox' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Select\Product\UseSeparateBox',
            self::SCHEMA_LABEL    => 'Ship in a separate box',
            self::SCHEMA_REQUIRED => false,
            self::SCHEMA_DEPENDENCY => array(
                self::DEPENDENCY_SHOW => array(
                    'shippable' => array(\XLite\View\FormField\Select\YesNo::YES),
                ),
            ),
        ),
        'enabled' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Checkbox\Enabled',
            self::SCHEMA_LABEL    => 'Available for sale',
            self::SCHEMA_REQUIRED => false,
        ),
        'arrivalDate' => array(
            self::SCHEMA_CLASS    => 'XLite\View\DatePicker',
            self::SCHEMA_LABEL    => 'Arrival date',
            \XLite\View\FormField\AFormField::PARAM_FIELD_ONLY => false,
            self::SCHEMA_REQUIRED => false,
        ),
        'meta_title' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Product page title',
            self::SCHEMA_REQUIRED => false,
        ),
        'brief_description' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Textarea\Advanced',
            self::SCHEMA_LABEL    => 'Brief description',
            self::SCHEMA_REQUIRED => false,
            \XLite\View\FormField\Textarea\Advanced::PARAM_STYLE => 'product-description',
        ),
        'description' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Textarea\Advanced',
            self::SCHEMA_LABEL    => 'Full description',
            self::SCHEMA_REQUIRED => false,
            \XLite\View\FormField\Textarea\Advanced::PARAM_STYLE => 'product-description',
        ),
        'meta_tags' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Meta keywords',
            self::SCHEMA_REQUIRED => false,
        ),
        'meta_desc' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text',
            self::SCHEMA_LABEL    => 'Meta description',
            self::SCHEMA_REQUIRED => false,
        ),
        'cleanURL' => array(
            self::SCHEMA_CLASS    => 'XLite\View\FormField\Input\Text\CleanURL',
            self::SCHEMA_LABEL    => 'Clean URL',
            self::SCHEMA_REQUIRED => false,
            \XLite\View\FormField\Input\Text\CleanURL::PARAM_OBJECT_CLASS_NAME  => '\XLite\Model\Product',
            \XLite\View\FormField\Input\Text\CleanURL::PARAM_OBJECT_ID_NAME     => 'product_id',
            \XLite\View\FormField\Input\Text\CleanURL::PARAM_ID                 => 'cleanurl',
        ),

    );
}
