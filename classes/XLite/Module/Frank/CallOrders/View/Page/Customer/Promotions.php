<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\Frank\CallOrders\View\Page\Customer;

/**
 * Site reviews page view
 *
 * @ListChild (list="center")
 */
class Promotions extends \XLite\View\AView
{
    /**
     * Return list of allowed targets
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        return array_merge(parent::getAllowedTargets(), array('promotions'));
    }

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/Frank/CallOrders/page/promotions/body.tpl';
    }

}
