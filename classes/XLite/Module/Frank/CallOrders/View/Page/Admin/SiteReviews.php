<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\Frank\CallOrders\View\Page\Admin;

/**
 * Call orders page view
 *
 * @ListChild (list="admin.center", zone="admin")
 */
class SiteReviews extends \XLite\View\AView
{
    /**
     * Return list of allowed targets
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        return array_merge(parent::getAllowedTargets(), array('site_reviews'));
    }

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/Frank/CallOrders/page/site_reviews/body.tpl';
    }

}
