<?php

namespace XLite\Module\Frank\CallOrders\View\Form\ItemsList\SiteReview;

class Table extends \XLite\View\Form\ItemsList\AItemsList
{
    protected function getDefaultTarget()
    {
        return 'site_reviews';
    }


    protected function getDefaultAction()
    {
        return 'update';
    }
}
