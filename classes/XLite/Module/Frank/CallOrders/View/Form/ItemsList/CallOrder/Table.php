<?php

namespace XLite\Module\Frank\CallOrders\View\Form\ItemsList\CallOrder;

class Table extends \XLite\View\Form\ItemsList\AItemsList
{
    protected function getDefaultTarget()
    {
        return 'call_orders';
    }


    protected function getDefaultAction()
    {
        return 'update';
    }
}
