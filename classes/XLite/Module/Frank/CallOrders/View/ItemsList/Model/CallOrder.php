<?php


namespace XLite\Module\Frank\CallOrders\View\ItemsList\Model;


class CallOrder extends \XLite\View\ItemsList\Model\Table
{

    protected function defineColumns()
    {
        return array(
            'name' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Name'),
                static::COLUMN_ORDERBY => 100,
            ),
            'phone' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Phone'),
                static::COLUMN_ORDERBY => 200,
            ),
            'processed' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Checkbox\Switcher\Enabled',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Is processed'),
                static::COLUMN_ORDERBY => 300,
            ),
        );
    }


    protected function defineRepositoryName()
    {
        return 'XLite\Module\Frank\CallOrders\Model\CallOrder';
    }


   protected function isSwitchable()
   {
       return false;
   }


   /**
    * Mark list as removable
    *
    * @return boolean
    */
   protected function isRemoved()
   {
       return false;
   }
}
