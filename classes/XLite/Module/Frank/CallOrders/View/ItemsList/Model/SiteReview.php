<?php


namespace XLite\Module\Frank\CallOrders\View\ItemsList\Model;


class SiteReview extends \XLite\View\ItemsList\Model\Table
{

    protected function defineColumns()
    {
        return array(
            'name' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Name'),
                static::COLUMN_ORDERBY => 100,
            ),
            'image' => array(
                self::COLUMN_CLASS    => 'XLite\View\FormField\Inline\FileUploader\Image',
                self::COLUMN_NAME => 'Image',
                static::COLUMN_ORDERBY => 150,
            ),
            'city' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('City'),
                static::COLUMN_ORDERBY => 200,
            ),
            'text' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Text'),
                static::COLUMN_ORDERBY => 300,
            ),
            'date' => array(
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Created at'),
                static::COLUMN_ORDERBY => 400,
            ),
            'published' => array(
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Checkbox\Switcher\Enabled',
                static::COLUMN_NAME => \XLite\Core\Translation::lbl('Is published'),
                static::COLUMN_ORDERBY => 500,
            ),
        );
    }


    protected function defineRepositoryName()
    {
        return 'XLite\Module\Frank\CallOrders\Model\SiteReview';
    }


   protected function isSwitchable()
   {
       return false;
   }


   /**
    * Mark list as removable
    *
    * @return boolean
    */
   protected function isRemoved()
   {
       return false;
   }
}
