<?php
namespace XLite\Module\UnisenderInc\Integration\View;

use XLite\Module\UnisenderInc\Integration;
use \XLite\View\Button;

/**
 * Subscribe form viewer
 */
class UnisenderForm extends \XLite\View\SideBarBox
{
    protected function getHead()
    {
        $title = \XLite\Core\Config::getInstance()->UnisenderInc->Integration->formTitle;
        if (empty($title)) {
            $title = static::t('subscribe');
        }
        return $title;
    }

    protected function getDir()
    {
        return 'modules/UnisenderInc/Integration';
    }

    public function getJSFiles()
    {
        $list = parent::getJSFiles();
        $list[] = 'modules/UnisenderInc/Integration/js/subscribeForm.js';

        return $list;
    }
}
