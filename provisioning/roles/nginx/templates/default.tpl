server {
    listen  80;

    root {{ doc_root }};
    index index.html index.php;

    server_name {{ servername }};

    location ~ (^|/)\. {
        return 403;
    }

    location / {
        if (!-e $request_filename) {
            rewrite ^/((([/_a-z0-9-]+)/)?([_a-z0-9-]+)/)?([_a-z0-9-]+)(\.(htm|html))?$ /index.php?url=$5&last=$4&rest=$3&ext=$7 last;
        }
    }

    location ~ \.php$ {
        # fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_read_timeout 300;
        fastcgi_index index.php;
        include fastcgi_params;
    }
}
