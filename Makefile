production_provision:
	ansible-playbook provisioning/playbook.yml -i provisioning/inventories/prod -vvvv
deploy:
	ansible-playbook provisioning/deploy.yml -i provisioning/inventories/prod -vvvv

ssh:
	ssh root@178.62.134.178

local_dump:
	mysqldump --user=lamarant --password=lamarant lamarant > db_dumps/dump.sql

remote_dump:
	ansible-playbook provisioning/remote_dump.yml -i provisioning/inventories/prod -vvvv

remote_image_dump:
	ansible-playbook provisioning/remote_images_dump.yml -i provisioning/inventories/prod -vvvv

restore_from_remote_images:
	tar -xvf ./dumps/178.62.134.178/tmp/lamarant/images/var_images.tar -C ./var/images && \
	tar -xvf ./dumps/178.62.134.178/tmp/lamarant/images/images.tar -C ./images

upload_local_dump:
	ansible-playbook provisioning/dump.yml -i provisioning/inventories/prod -vvvv


restore_from_remote_dump:
	mysql --user=lamarant --password=lamarant lamarant < ./db_dumps/178.62.134.178/tmp/lamarant/db_dumps/dump.sql

clear_cache:
	rm -rf var/run

